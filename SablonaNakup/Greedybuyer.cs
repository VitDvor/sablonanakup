﻿using System;
using System.Collections.Generic;

namespace SablonaNakup
{
    class Greedypamet : Nakupujici
    { //jde vzdy za prvni nejlepsi nabidkou, utíka pred zlodeji
        public override Smer Krok(char[,] castMapy, int penize, Souradnice mojeSouradnice)
        {
            int mesec = penize;
            List<int> sirkapoklad = new List<int>();
            List<int> vyskapoklad = new List<int>();
            List<int> hodnotapokladu = new List<int>();
            List<int> sirkazlocinec = new List<int>();
            List<int> vyskazlocinec = new List<int>();
            List<int> stenax = new List<int>();
            List<int> stenay = new List<int>();
            int stena = 0;
            int x = 0; //sirka kam pujdu
            int y = 0; //vyska kam pujdu
            int i = 0; //index array poklad
            int zloc = 0; //index array zlocinec
            // vzdy resetuji protoze potrebuji pracovat s aktualnim stavem
            int vyskalim = 4;
            int sirkalim = 4;
            int vyskalim0 = 0;
            int sirkalim0 = 0; //at nekoukam do zdi.
            int hodnotapole = 0;
            if (mojeSouradnice.X == 80)
            {
                sirkalim = 2;
            }
            if (mojeSouradnice.Y == 20)
            {
                vyskalim = 2;
            }
            if (mojeSouradnice.X == 0)
            {
                sirkalim = 2;
            }
            if (mojeSouradnice.Y == 0)
            {
                sirkalim = 2;
            }

            for (int vyska = vyskalim0; vyska <= vyskalim; vyska++) //prohledam vsude kde vidim abych nasel prvni nejlepsi nabidku
            {
                for (int sirka = sirkalim0; sirka <= sirkalim; sirka++)
                {
                    var i21 = Char.GetNumericValue(castMapy[sirka, vyska]);
                    hodnotapole = Convert.ToInt32(i21);
                    if (hodnotapole > 0 && hodnotapole < 10)
                    {
                        if (mesec < 100 && hodnotapole < 5)
                        {
                            sirkapoklad[i] = sirka;
                            vyskapoklad[i] = vyska;
                            hodnotapokladu[i] = hodnotapole;
                            i++;

                        }

                        if (mesec < 50 && hodnotapole < 7)
                        {
                            sirkapoklad[i] = sirka;
                            vyskapoklad[i] = vyska;
                            hodnotapokladu[i] = hodnotapole;
                            i++;
                        }
                        if (mesec < 30 && hodnotapole < 9)
                        {
                            sirkapoklad[i] = sirka;
                            vyskapoklad[i] = vyska;
                            hodnotapokladu[i] = hodnotapole;
                            i++;
                        }
                    }
                    if (hodnotapole > 64 && hodnotapole < 87 || hodnotapole > 88 && hodnotapole < 91)
                    {
                        sirkazlocinec[zloc] = sirka;
                        vyskazlocinec[zloc] = vyska; //nactu si zlocince
                    }
                    if (hodnotapole == 88)
                    {
                        stenax[stena] = sirka;
                        stenay[stena] = vyska;
                    }
                }
            }
            // zacina delat krok.

            if (zloc != 0)
            { //je-li, hleda unikovou cestu
                int x2 = 0;
                int y2 = 0;
                for (int cy = 0; cy < zloc; cy++)
                {
                    if (sirkazlocinec[cy] < 3) { x2 = x2 + 1; }
                    if (vyskazlocinec[cy] < 3) { y2 = y2 + 1; }
                    if (sirkazlocinec[cy] > 3) { x2 = x2 - 1; }
                    if (vyskazlocinec[cy] > 3) { y2 = y2 - 1; }
                    if (x2 == 0) //mam-li dva zlodeje proti sobe
                    {
                        x = x2;
                        if (y2 > 0) { y = -1; }
                        if (y2 < 0) { y = 1; }
                        for (int stenacheck = 0; stenacheck < stena; stenacheck++)
                        {
                            if (x == stenax[stenacheck] && y == stenay[stenacheck])
                            {

                                if (nahoda.Next(5) <= 2)
                                {
                                    y = 0;

                                }
                                if (nahoda.Next(5) > 2)
                                {
                                    x = 0;

                                }

                            }
                        }
                        return new Smer((sbyte)(x), (sbyte)(y));
                    }
                    if (y2 == 0) //mam-li dva zlodeje proti sobe
                    {
                        y = y2;
                        if (x2 > 0) { x = -1; }
                        if (x2 < 0) { x = 1; }
                        return new Smer((sbyte)(x), (sbyte)(y));
                    }

                } //utíkám tam, kde je zlodeju nejmene.
                if (x2 > 0) { x = -1; }
                if (x2 < 0) { x = 1; }
                if (y2 > 0) { y = -1; }
                if (y2 < 0) { y = 1; }
                return new Smer((sbyte)(x), (sbyte)(y));
            }
            else
            {
                int Nahoda;
                do
                {
                    Nahoda = nahoda.Next(3) - 1;
                } while (Nahoda == 0);
                x = Nahoda;
                do
                {
                    Nahoda = nahoda.Next(3) - 1;
                } while (Nahoda == 0);
                y = Nahoda;
                for (int stenacheck = 0; stenacheck < stena; stenacheck++)
                {
                    if (x == stenax[stenacheck] && y == stenay[stenacheck])
                    {

                        if (nahoda.Next(1, 2) == 2)
                        {
                            y = y * -1;

                        }
                        if (nahoda.Next(1, 2) == 1)
                        {
                            x = x * -1;

                        }

                    }
                }

                return new Smer((sbyte)(x), (sbyte)(y));
                //staneli se chyba, jdu nahodne, at se aspon neco
            }
        }
    }
}