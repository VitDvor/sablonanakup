﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SablonaNakup
{

    class PathfindingZlodej : Zlodej
    {


        public class RobbedBastards //when we estimate all the places where a missing shopper could be, we need all this info
        {
            public RobbedBastards(char identify, int x, int y, int step)
            {
                identity = identify;
                X = x;
                Y = y;
                this.step = step - 1; // we are forced to wait a turn when we rob a man, and that turn does not get added in here
            }

            public char identity;
            public int X;
            public int Y;
            public int step;
        }

        public AstarPathfindingGraph g;

        Node[,] nodemap;

        //Make sure the map size is correct!
        int mapX = 80;
        int mapY = 20;

        int step = 0; //current turn number
        int waitUntil = 0; //in case it is more efficient no to move
        char justRobbed = '-'; //'-' means noone

        int lastMoney = 0; // money we just had before robbing someone used to get lastRobberyMoney
        int lastRobberyMoney = 0; //a variable we get later on by substracting lastMoney from our score

        List<RobbedBastards> robbed; //list of people that are hidden from us
        List<RobbedBastards> estimations = new List<RobbedBastards>(); //list of all spots where a hidden shopper might be
        List<char> shoppersWithoutMoney; //if we rob someone and we do not get 5 gold, he is not worth chasing

        char[,] oldMap;// I need this when checking for invisible shoppers

        public PathfindingZlodej()
        { //just the constructor, nothing fancy here, inicializing the variables
            g = new AstarPathfindingGraph();
            robbed = new List<RobbedBastards>();
            shoppersWithoutMoney = new List<char>();
        }

        public double GetDistance(int X, int x, int Y, int y) //a function which gets you the distance of two places on the map, considering the diagonal of a 1 sized square is 1 not the root of 2; Normally, the shortest path from point A to point B is a straight line. Since we cannot go in a straight line
        {

            if (Math.Abs(X - x) > Math.Abs(Y - y))
            {
                return Math.Abs(X - x);//the part we cross diagonally is as long as we can cross both the X and Y axis at once AKA the length of the shorter one; the rest is moving horizonatally/vertically BUT the sum of these parts is always going to be the bigger axis distance. Kinda cool
            }
            else
            {
                return Math.Abs(Y - y);
            }


        }

        public override Smer Krok(char[,] castMapy, int penize, Souradnice souradnice, int opakovani) //the function the gamemanager calls
        {

            if (opakovani > 1) { return new Smer(0, 0); }//if we hit a buyer, we stand still

            //Up the step
            step++;

            //Clear nodemap, recreating does not take too long and making a new one is simpler than checking all the changes and chainging it accordingly
            nodemap = new Node[mapX, mapY];

            //CLear graph vertices
            g.ClearVertices();

            //Distances to other shoppers <shopper identifier, <xyCoords, distance>> THIS IS NOT HOW MANY TURNS IT TAKES, JUST HOW long a straight line connecting them is
            Dictionary<char, KeyValuePair<int[], double>> shoppers = new Dictionary<char, KeyValuePair<int[], double>>();

            /*Parse the map data
             * 'X' == A wall
             * 'capitalized letter' == A thief
             * 'lower-case letter' == A shopper
             * '0-9" == An offer
             */
            for (int y = 0; y < mapY; y++)
            {
                for (int x = 0; x < mapX; x++)
                {
                    char c = castMapy[x, y];

                    if (c == 'X' || System.Char.IsDigit(c) || System.Char.IsUpper(c))
                    {
                        //Do nothing, walls, offers and other thieves are unreachable to us
                    }

                    //Keep track of shopper positions and distances
                    if (System.Char.IsLower(c))
                    { //if the tile is a shopper
                        shoppers.Add(
                            c,//the shopper identity
                              // new KeyValuePair<int[], double>(new int[] { x, y }, Math.Sqrt(Math.Pow(x - souradnice.X, 2) + Math.Pow(y - souradnice.Y, 2)))//their x,y co-ords and calculated distance
                            new KeyValuePair<int[], double>(new int[] { x, y }, GetDistance(souradnice.X, x, souradnice.Y, y)) //more effective
                            );
                    }

                    //Can move onto tile if its empty, if it is the one I'm standing on or if the tile is a shopper, since we want to move into him
                    //We add such places into our "node-map" which the searching algorithm uses
                    if (c == ' ' || (x == souradnice.X && y == souradnice.Y) || System.Char.IsLower(c))
                    {
                        Node n = new Node(x, y, this);
                        nodemap[x, y] = n;

                        /*Connect n to all surrounding nodes if possible
                        Duplicate connections and nonexisting nodes are not a problem since Node.connectTo ignores them*/
                        n.connectTo(getNodeAt(x + 1, y));
                        n.connectTo(getNodeAt(x - 1, y));
                        n.connectTo(getNodeAt(x, y + 1));
                        n.connectTo(getNodeAt(x, y - 1));
                        n.connectTo(getNodeAt(x + 1, y + 1));
                        n.connectTo(getNodeAt(x + 1, y - 1));
                        n.connectTo(getNodeAt(x - 1, y + 1));
                        n.connectTo(getNodeAt(x - 1, y - 1));

                        g.AddVertex(n); //actually adding the node
                    }
                }
            }

            //First, if we robbed someone in the last step, check the money difference and refresh the justRobbedSomeone boolean
            if (justRobbed != '-')
            {
                //Get the money difference
                lastRobberyMoney = penize - lastMoney;

                //If we stole less than 5, it means the shopper has no money now
                if (lastRobberyMoney < 5)
                {
                    shoppersWithoutMoney.Add(justRobbed);
                }

                //Reset justRobbed
                justRobbed = '-';
                oldMap = castMapy; //If I did not do this I would later compare a 2 turn old map with the current one, which is unacceptable and would give really bad info
            }
            lastMoney = penize;

            //If there are no shoppers visible, wait
            if (shoppers.Count == 0)
            {
                oldMap = castMapy;
                return new Smer((sbyte)0, (sbyte)0);
            }

            //Make sure that visible shoppers are not in the robbed list (because we can see them again)
            if (robbed.Count > 0)
            {

                List<RobbedBastards> toRemove = new List<RobbedBastards>();//While it might be unnecessary to assume more than one shopper could become visible in a turn, we make sure we do not run into any issues and assume more than one could
                foreach (char s in shoppers.Keys) //We go through each shopper in the shoppers list
                {
                    foreach (RobbedBastards c in robbed) //we compare it to every shopper in the robbed list
                    {
                        if (s == c.identity)
                        {
                            toRemove.Add(c); //If a shopper is in both lists, it means he just became visible
                        }
                    }

                }
                foreach (RobbedBastards r in toRemove)//YES, IT DOES seem retarded not to be able to just remove it right away but because of the way the damn list is modified, we have to do it like this, in two cycles; yes, I was looking for this bug for a long time; MOREOVER I compared different things every time, meaning I can not even make my own function and have to remake this every,single,time
                {
                    robbed.Remove(r);
                }
            }

            if (robbed.Count > 0)
            {
                //if there are still buyers we do not see, we try to estimate where they might be due to disappearing offers
                List<RobbedBastards> toRemove = new List<RobbedBastards>();
                foreach (KeyValuePair<char, KeyValuePair<int[], double>> temp in shoppers)//we remove all astimations of visible shoppers
                {
                    foreach (RobbedBastards R in estimations)
                    {
                        if (temp.Key == R.identity)
                        {
                            toRemove.Add(R);
                        }
                    }
                }
                foreach (RobbedBastards r in toRemove)
                {
                    robbed.Remove(r);
                }
                toRemove = new List<RobbedBastards>();

                for (int xcord = 0; xcord < mapX; xcord++)
                {
                    for (int ycord = 0; ycord < mapY; ycord++)
                    {
                        if (castMapy[xcord, ycord] != oldMap[xcord, ycord])
                        {
                            if (Char.IsDigit(oldMap[xcord, ycord]) && castMapy[xcord, ycord] == ' ') //we check if any offers mysteriously disappeared
                            {
                                char buy = '0';
                                int dist = int.MaxValue;
                                foreach (RobbedBastards potential in robbed) //we find the closest missing shopper and assign him this mystery
                                {
                                    if (GetDistance(potential.X, xcord, potential.Y, ycord) < dist)
                                    {
                                        dist = (int)GetDistance(potential.X, xcord, potential.Y, ycord);
                                        buy = potential.identity;
                                    }
                                }
                                //when we find the closest person to the suspicious spot we add him to the estimations list
                                foreach (RobbedBastards R in estimations)
                                {
                                    if (R.identity == buy) { toRemove.Add(R); }
                                }
                                foreach (RobbedBastards r in toRemove)
                                {
                                    estimations.Remove(r);
                                }
                                toRemove = new List<RobbedBastards>();
                                estimations.Add(new RobbedBastards(buy, xcord, ycord, step));
                            }

                        }
                    }
                }

                foreach (char n in shoppersWithoutMoney)
                {
                    foreach (RobbedBastards c in robbed)
                    {
                        if (n == c.identity) { toRemove.Add(c); }
                    }
                    foreach (RobbedBastards c in toRemove) //Just in case we robbed someone we don't want to track
                    {
                        estimations.Remove(c);   //remove the estimation of the distance is reachable by the shopper we do not wish to rob again
                    }
                    toRemove = new List<RobbedBastards>();
                }

                foreach (RobbedBastards R in estimations)
                {
                    if (!(shoppers.Keys.Contains(R.identity)))
                    {
                        shoppers.Add(
                            R.identity,//the shopper identity
                            new KeyValuePair<int[], double>(new int[] { R.X, R.Y }, GetDistance(souradnice.X, R.X, souradnice.Y, R.Y)) // x,y co-ords and calculated distance
                        );
                    }
                }

                oldMap = castMapy; //we do not need to change the old map in any other case. We change it when we detect we just robbbed someone AND whenever there is an invisible shopper
            }
            else
            {
                //if there are no missing shoppers, we clear the list of all predictions simply by making a new, empty one
                estimations = new List<RobbedBastards>();
            }

            /*Getting the path to the closest shopper. If the path cannot be made (walls in the way for example),
             *we are going to repeat the process and get the path to the 2nd closest shopper, and so on
             */
            int searchPosition = 1;
            KeyValuePair<int[], double> closestShopper = new KeyValuePair<int[], double>();
            Node currentNode = null;
            Node target = null;
            List<Node> path = null;
            int pathSteps = int.MaxValue;

            while (searchPosition <= shoppers.Count)
            {
                closestShopper = FindClosestShopper(shoppers, searchPosition);
                searchPosition++;

                //If the closest shopper has no money, continue and check the next shopper in line (2nd closest shopper and so on)
                if (this.shoppersWithoutMoney.Contains(castMapy[closestShopper.Key[0], closestShopper.Key[1]]))
                {
                    continue;
                }

                //Node we are on
                currentNode = getNodeAt(souradnice.X, souradnice.Y);

                //Sets the closest shopper as a path target           
                target = getNodeAt(closestShopper.Key[0], closestShopper.Key[1]);

                path = null;
                pathSteps = int.MaxValue;

                //Get the path to the closest shopper
                if (currentNode != null && target != null && currentNode != target)
                {
                    path = g.ShortestPathPriorityAstar(currentNode, target);
                    if (path != null)
                    {
                        pathSteps = path.Count;
                        break;
                    }
                }
            }

            /*In the case that some shoppers are robbed and we can't see them
             * and the closest visible shopper is more than 20 steps away (or the paths to all other shoppers are obstructed), ##Notice
             * and there is a pending wait,
             * --> stay in place and wait for the robbed shoppers to reappear
             *
             * ##Notice: It would take us a minimal of 21 steps (if he isn't moving towards us) to get to the closest visible shopper.
             * And we know that if we just robbed a shopper, he is going to reapear a maximum of 10 steps away, which means 10 more steps for us to take (=20)
             * So it's more efficent for us to wait in place for the robbed shopper to reappear. (As long as he has enough money
             */

            if (pathSteps > 20 && robbed.Count > 0 && waitUntil > step)
            {
                return new Smer((sbyte)0, (sbyte)0);
            }

            if (path != null)
            {
                if (path.Count != 0)
                {
                    //Gets the first path node
                    Node nextStepNode = path[path.Count - 1];

                    //And moves towards it
                    int finalMoveX = nextStepNode.getX() - currentNode.getX();
                    int finalMoveY = nextStepNode.getY() - currentNode.getY();

                    //Check if we're robbing someone
                    if (System.Char.IsLower(castMapy[souradnice.X + finalMoveX, souradnice.Y + finalMoveY]))
                    {
                        //The robbed shopper is added to the robbed list (this means we wont see him for 10 turns), we also mark him as a shopper we just robbed
                        RobbedBastards temp = new RobbedBastards(castMapy[souradnice.X + finalMoveX, souradnice.Y + finalMoveY], souradnice.X + finalMoveX, souradnice.Y + finalMoveY, step);

                        robbed.Add(temp);
                        justRobbed = castMapy[souradnice.X + finalMoveX, souradnice.Y + finalMoveY];

                        //If we arent waiting already, set a 9 step wait + we wait 1 turn no matter what, since we robbed him
                        if (waitUntil < step)
                        {
                            waitUntil = step + 11;
                        }
                    }

                    //The path is recalculated every turn so the first step of each path will ultimately lead the thief towards the target
                    return new Smer((sbyte)finalMoveX, (sbyte)finalMoveY);
                }
            }
            return new Smer((sbyte)0, (sbyte)0);
        }

        //Sorts shoppers by distance using the magic of delegates, the position specifes the position in the sorted list (=> 1 returns the 1st closest shopper, 2 returns the 2nd)
        KeyValuePair<int[], double> FindClosestShopper(Dictionary<char, KeyValuePair<int[], double>> shoppers, int position)
        {
            List<KeyValuePair<char, KeyValuePair<int[], double>>> tempList = new List<KeyValuePair<char, KeyValuePair<int[], double>>>(shoppers);
            tempList.Sort(
                delegate (KeyValuePair<char, KeyValuePair<int[], double>> firstPair,
                KeyValuePair<char, KeyValuePair<int[], double>> nextPair)
                {
                    return firstPair.Value.Value.CompareTo(nextPair.Value.Value);
                }
            );
            return tempList[position - 1].Value;
        }

        //Utility function that makes sure we don't exceed the map limits
        Node getNodeAt(int x, int y)
        {
            if (x < 0 || y < 0 || x > mapX - 1 || y > mapY - 1)
            {
                return null;
            }
            return nodemap[x, y];
        }

    }

    class AstarPathfindingGraph
    {
        List<Node> nodelist = new List<Node>();

        public void AddVertex(Node n) //add a node in the list
        {
            nodelist.Add(n);
        }

        public void RemoveVertex(Node n) //remove a node
        {
            nodelist.Remove(n);
        }

        public void ClearVertices() //remove all nodes
        {
            nodelist.Clear();
        }

        public List<Node> getNodelist() //returns the list of nodes
        {
            return nodelist;
        }

        //A* (A star) search algorithm implementation using a priority queue data structure.

        /*A star is an "extension" to regular Dijkstra algorithm and uses heuristics which are hints that guide the algorithm to explore nodes that tend to be in the direction of the target
         * which results in better performance
         * This implementation uses Euclidean distance as a heuristic. (eg. distance from the target node)         
         *
         * A* does not always find the shortest path, it trades precision for performance which is needed to achieve a good time complexity in dense graphs (8 directional grids).
         * But since the map in this application is random, paths are not very complex, so A* works just fine.
         */
        public List<Node> ShortestPathPriorityAstar(Node start, Node finish)
        {
            /*The simplest dijkstra implementation can use a regular List<Node>, I've tried that and the algorithm performs horribly (O(n^2)),
             *so that's why I'm using a PriorityQueue that performs much better than a List<T>.
             */
            SimplePriorityQueue<Node> priorityQueue = new SimplePriorityQueue<Node>();

            //Using a HashSet to check whether a node has been already visited because it has constant .contains() time complexity (O(1))
            HashSet<Node> alreadyVisited = new HashSet<Node>();

            //The returned path, the first path found is returned (it does not keep searching <- because performance)
            List<Node> path = null;
            bool pathFound = false;

            //Resets the node distances and heuristics, distances and heuristics set to infinity (int.Max in this case)
            foreach (Node n in nodelist)
            {
                //The start node distance is 0 and the heuristic is the vector lenght between start and end node coordinates
                if (n == start)
                {
                    n.distance = 0;
                    n.heuristic = (float)(Math.Sqrt(Math.Pow(start.getX() - finish.getX(), 2) + Math.Pow(start.getY() - finish.getY(), 2)));
                }
                else
                {
                    n.distance = int.MaxValue;
                    n.heuristic = int.MaxValue;
                }
            }

            priorityQueue.Enqueue(start, start.heuristic);

            //Keep searching until you've run out of nodes or a path has been found
            while (priorityQueue.Count != 0 || !pathFound)
            {
                //Getting the first queue element, .TryDequeue is used to prevent exceptions when no path can be found
                Node smallest;
                if (priorityQueue.TryDequeue(out smallest) == false)
                {
                    break;
                };

                alreadyVisited.Add(smallest);

                //If the top node in the priorityQueue is the target node, start tracing back the path, break the outer loop and end the search.
                if (smallest == finish)
                {
                    path = new List<Node>();
                    pathFound = true;
                    while (smallest != start)
                    {
                        path.Add(smallest);
                        smallest = smallest.previous;
                    }
                    break;
                }

                if (smallest.distance == int.MaxValue)
                {
                    break;
                }

                foreach (Node neighbor in smallest.getAllConnectedNodes())
                {
                    //Check if this node was not visited already (if so, skip it) this should prevent looping (and thus undefined behaviour)
                    if (alreadyVisited.Contains(neighbor))
                    {
                        continue;
                    }

                    //A* heuristic, heurDist = current chain step count + weight 1 (because the grid is uniform) + euclidean distance from the target to the current node
                    float heurDist = (float)(smallest.distance + 1 + (Math.Sqrt(Math.Pow(neighbor.getX() - finish.getX(), 2) + Math.Pow(neighbor.getY() - finish.getY(), 2))));
                    float dist = smallest.distance + 1;

                    if (dist < neighbor.distance)
                    {
                        neighbor.distance = dist;
                        neighbor.heuristic = heurDist;
                        neighbor.previous = smallest;

                        if (priorityQueue.Contains(neighbor))
                        {
                            priorityQueue.UpdatePriority(neighbor, neighbor.heuristic);
                        }
                        else
                        {
                            priorityQueue.Enqueue(neighbor, neighbor.heuristic);
                        }
                    }
                }
            }

            return path;
        }
    }

    class Node //THe class defining the Nodes our internal map, on which we can move, and which we use to find a path through
    {
        PathfindingZlodej zlodej;
        int x, y;

        List<Node> connectedTo;
        List<Node> connectedBy;

        //for pathfinding
        public float distance = int.MaxValue;
        public float heuristic = int.MaxValue;
        public Node previous = null;

        public Node(int x, int y, PathfindingZlodej zlodej)
        {
            this.zlodej = zlodej;
            this.x = x;
            this.y = y;

            connectedTo = new List<Node>();
            connectedBy = new List<Node>();
        }

        public void disconnectFromAll() //removes all connections, used when clearing the nodemap
        {
            foreach (Node n in connectedTo)
            {
                n.connectedBy.Remove(this);
            }
            foreach (Node n in connectedBy)
            {
                n.connectedTo.Remove(this);
            }
            this.connectedBy.Clear();
            this.connectedTo.Clear();
        }

        public void connectTo(Node targetnode) //adds a connection to a node from this node and a connecton from the other node to this one
        {
            if (targetnode != null)
            {
                if (!(connectedBy.Contains(targetnode)) && !(connectedTo.Contains(targetnode)))
                {
                    connectedTo.Add(targetnode);
                    targetnode.connectedBy.Add(this);
                }
            }
        }

        public List<Node> getAllConnectedNodes() //returns a list of all Nodes
        {
            List<Node> mergedArray = new List<Node>();
            mergedArray.AddRange(connectedTo);
            mergedArray.AddRange(connectedBy);
            return mergedArray;
        }

        public int getIndex()
        {
            return zlodej.g.getNodelist().IndexOf(this);
        }

        public int getX()//returns the X co-ordinate of this node
        {
            return x;
        }

        public int getY()//returns the y co-ordinate of this node
        {
            return y;
        }
    }

    //I need a priority queue data structure to create an effective pathfinding algorithm that would not freeze the whole application when working with more than 1000 nodes.
    //Both Java and C++ have a PriorityQueue as part of their basic API, so I think that using a free third party implementation is justifiable.
    //When when working with 40 000 nodes (map size of 200x200), regular simple implementations of Dijikstra or A* just give up
    #region PriorityQueue - Priority queue implementation by BlueRaja (https://github.com/BlueRaja/High-Speed-Priority-Queue-for-C-Sharp). Licensed under the MIT License (https://opensource.org/licenses/MIT).

    /// <summary>
    /// The IPriorityQueue interface.  This is mainly here for purists, and in case I decide to add more implementations later.
    /// For speed purposes, it is actually recommended that you *don't* access the priority queue through this interface, since the JIT can
    /// (theoretically?) optimize method calls from concrete-types slightly better.
    /// </summary>
    public interface IPriorityQueue<TItem, in TPriority> : IEnumerable<TItem>
        where TPriority : IComparable<TPriority>
    {
        /// <summary>
        /// Enqueue a node to the priority queue.  Lower values are placed in front. Ties are broken by first-in-first-out.
        /// See implementation for how duplicates are handled.
        /// </summary>
        void Enqueue(TItem node, TPriority priority);

        /// <summary>
        /// Removes the head of the queue (node with minimum priority; ties are broken by order of insertion), and returns it.
        /// </summary>
        TItem Dequeue();

        /// <summary>
        /// Removes every node from the queue.
        /// </summary>
        void Clear();

        /// <summary>
        /// Returns whether the given node is in the queue.
        /// </summary>
        bool Contains(TItem node);

        /// <summary>
        /// Removes a node from the queue.  The node does not need to be the head of the queue.  
        /// </summary>
        void Remove(TItem node);

        /// <summary>
        /// Call this method to change the priority of a node.  
        /// </summary>
        void UpdatePriority(TItem node, TPriority priority);

        /// <summary>
        /// Returns the head of the queue, without removing it (use Dequeue() for that).
        /// </summary>
        TItem First { get; }

        /// <summary>
        /// Returns the number of nodes in the queue.
        /// </summary>
        int Count { get; }
    }

    /// <summary>
    /// A helper-interface only needed to make writing unit tests a bit easier (hence the 'internal' access modifier)
    /// </summary>
    internal interface IFixedSizePriorityQueue<TItem, in TPriority> : IPriorityQueue<TItem, TPriority>
        where TPriority : IComparable<TPriority>
    {
        /// <summary>
        /// Resize the queue so it can accept more nodes.  All currently enqueued nodes are remain.
        /// Attempting to decrease the queue size to a size too small to hold the existing nodes results in undefined behavior
        /// </summary>
        void Resize(int maxNodes);

        /// <summary>
        /// Returns the maximum number of items that can be enqueued at once in this queue.  Once you hit this number (ie. once Count == MaxSize),
        /// attempting to enqueue another item will cause undefined behavior.
        /// </summary>
        int MaxSize { get; }
    }

    public class GenericPriorityQueueNode<TPriority>
    {
        /// <summary>
        /// The Priority to insert this node at.  Must be set BEFORE adding a node to the queue (ideally just once, in the node's constructor).
        /// Should not be manually edited once the node has been enqueued - use queue.UpdatePriority() instead
        /// </summary>
        public TPriority Priority { get; protected internal set; }

        /// <summary>
        /// Represents the current position in the queue
        /// </summary>
        public int QueueIndex { get; internal set; }

        /// <summary>
        /// Represents the order the node was inserted in
        /// </summary>
        public long InsertionIndex { get; internal set; }
    }

    /// <summary>
    /// A copy of StablePriorityQueue which also has generic priority-type
    /// </summary>
    /// <typeparam name="TItem">The values in the queue.  Must extend the GenericPriorityQueue class</typeparam>
    /// <typeparam name="TPriority">The priority-type.  Must extend IComparable&lt;TPriority&gt;</typeparam>
    public sealed class GenericPriorityQueue<TItem, TPriority> : IFixedSizePriorityQueue<TItem, TPriority>
        where TItem : GenericPriorityQueueNode<TPriority>
        where TPriority : IComparable<TPriority>
    {
        private int _numNodes;
        private TItem[] _nodes;
        private long _numNodesEverEnqueued;
        private readonly Comparison<TPriority> _comparer;

        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        /// <param name="maxNodes">The max nodes ever allowed to be enqueued (going over this will cause undefined behavior)</param>
        public GenericPriorityQueue(int maxNodes) : this(maxNodes, Comparer<TPriority>.Default) { }

        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        /// <param name="maxNodes">The max nodes ever allowed to be enqueued (going over this will cause undefined behavior)</param>
        /// <param name="comparer">The comparer used to compare TPriority values.</param>
        public GenericPriorityQueue(int maxNodes, IComparer<TPriority> comparer) : this(maxNodes, comparer.Compare) { }

        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        /// <param name="maxNodes">The max nodes ever allowed to be enqueued (going over this will cause undefined behavior)</param>
        /// <param name="comparer">The comparison function to use to compare TPriority values</param>
        public GenericPriorityQueue(int maxNodes, Comparison<TPriority> comparer)
        {
#if DEBUG
            if (maxNodes <= 0)
            {
                throw new InvalidOperationException("New queue size cannot be smaller than 1");
            }
#endif

            _numNodes = 0;
            _nodes = new TItem[maxNodes + 1];
            _numNodesEverEnqueued = 0;
            _comparer = comparer;
        }

        /// <summary>
        /// Returns the number of nodes in the queue.
        /// O(1)
        /// </summary>
        public int Count
        {
            get
            {
                return _numNodes;
            }
        }

        /// <summary>
        /// Returns the maximum number of items that can be enqueued at once in this queue.  Once you hit this number (ie. once Count == MaxSize),
        /// attempting to enqueue another item will cause undefined behavior.  O(1)
        /// </summary>
        public int MaxSize
        {
            get
            {
                return _nodes.Length - 1;
            }
        }

        /// <summary>
        /// Removes every node from the queue.
        /// O(n) (So, don't do this often!)
        /// </summary>
#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Clear()
        {
            Array.Clear(_nodes, 1, _numNodes);
            _numNodes = 0;
        }

        /// <summary>
        /// Returns (in O(1)!) whether the given node is in the queue.  O(1)
        /// </summary>
#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public bool Contains(TItem node)
        {
#if DEBUG
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }
            if (node.QueueIndex < 0 || node.QueueIndex >= _nodes.Length)
            {
                throw new InvalidOperationException("node.QueueIndex has been corrupted. Did you change it manually? Or add this node to another queue?");
            }
#endif

            return (_nodes[node.QueueIndex] == node);
        }

        /// <summary>
        /// Enqueue a node to the priority queue.  Lower values are placed in front. Ties are broken by first-in-first-out.
        /// If the queue is full, the result is undefined.
        /// If the node is already enqueued, the result is undefined.
        /// O(log n)
        /// </summary>
#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Enqueue(TItem node, TPriority priority)
        {
#if DEBUG
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }
            if (_numNodes >= _nodes.Length - 1)
            {
                throw new InvalidOperationException("Queue is full - node cannot be added: " + node);
            }
            if (Contains(node))
            {
                throw new InvalidOperationException("Node is already enqueued: " + node);
            }
#endif

            node.Priority = priority;
            _numNodes++;
            _nodes[_numNodes] = node;
            node.QueueIndex = _numNodes;
            node.InsertionIndex = _numNodesEverEnqueued++;
            CascadeUp(node);
        }

#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        private void CascadeUp(TItem node)
        {
            //aka Heapify-up
            int parent;
            if (node.QueueIndex > 1)
            {
                parent = node.QueueIndex >> 1;
                TItem parentNode = _nodes[parent];
                if (HasHigherPriority(parentNode, node))
                    return;

                //Node has lower priority value, so move parent down the heap to make room
                _nodes[node.QueueIndex] = parentNode;
                parentNode.QueueIndex = node.QueueIndex;

                node.QueueIndex = parent;
            }
            else
            {
                return;
            }
            while (parent > 1)
            {
                parent >>= 1;
                TItem parentNode = _nodes[parent];
                if (HasHigherPriority(parentNode, node))
                    break;

                //Node has lower priority value, so move parent down the heap to make room
                _nodes[node.QueueIndex] = parentNode;
                parentNode.QueueIndex = node.QueueIndex;

                node.QueueIndex = parent;
            }
            _nodes[node.QueueIndex] = node;
        }

#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        private void CascadeDown(TItem node)
        {
            //aka Heapify-down
            int finalQueueIndex = node.QueueIndex;
            int childLeftIndex = 2 * finalQueueIndex;

            // If leaf node, we're done
            if (childLeftIndex > _numNodes)
            {
                return;
            }

            // Check if the left-child is higher-priority than the current node
            int childRightIndex = childLeftIndex + 1;
            TItem childLeft = _nodes[childLeftIndex];
            if (HasHigherPriority(childLeft, node))
            {
                // Check if there is a right child. If not, swap and finish.
                if (childRightIndex > _numNodes)
                {
                    node.QueueIndex = childLeftIndex;
                    childLeft.QueueIndex = finalQueueIndex;
                    _nodes[finalQueueIndex] = childLeft;
                    _nodes[childLeftIndex] = node;
                    return;
                }
                // Check if the left-child is higher-priority than the right-child
                TItem childRight = _nodes[childRightIndex];
                if (HasHigherPriority(childLeft, childRight))
                {
                    // left is highest, move it up and continue
                    childLeft.QueueIndex = finalQueueIndex;
                    _nodes[finalQueueIndex] = childLeft;
                    finalQueueIndex = childLeftIndex;
                }
                else
                {
                    // right is even higher, move it up and continue
                    childRight.QueueIndex = finalQueueIndex;
                    _nodes[finalQueueIndex] = childRight;
                    finalQueueIndex = childRightIndex;
                }
            }
            // Not swapping with left-child, does right-child exist?
            else if (childRightIndex > _numNodes)
            {
                return;
            }
            else
            {
                // Check if the right-child is higher-priority than the current node
                TItem childRight = _nodes[childRightIndex];
                if (HasHigherPriority(childRight, node))
                {
                    childRight.QueueIndex = finalQueueIndex;
                    _nodes[finalQueueIndex] = childRight;
                    finalQueueIndex = childRightIndex;
                }
                // Neither child is higher-priority than current, so finish and stop.
                else
                {
                    return;
                }
            }

            while (true)
            {
                childLeftIndex = 2 * finalQueueIndex;

                // If leaf node, we're done
                if (childLeftIndex > _numNodes)
                {
                    node.QueueIndex = finalQueueIndex;
                    _nodes[finalQueueIndex] = node;
                    break;
                }

                // Check if the left-child is higher-priority than the current node
                childRightIndex = childLeftIndex + 1;
                childLeft = _nodes[childLeftIndex];
                if (HasHigherPriority(childLeft, node))
                {
                    // Check if there is a right child. If not, swap and finish.
                    if (childRightIndex > _numNodes)
                    {
                        node.QueueIndex = childLeftIndex;
                        childLeft.QueueIndex = finalQueueIndex;
                        _nodes[finalQueueIndex] = childLeft;
                        _nodes[childLeftIndex] = node;
                        break;
                    }
                    // Check if the left-child is higher-priority than the right-child
                    TItem childRight = _nodes[childRightIndex];
                    if (HasHigherPriority(childLeft, childRight))
                    {
                        // left is highest, move it up and continue
                        childLeft.QueueIndex = finalQueueIndex;
                        _nodes[finalQueueIndex] = childLeft;
                        finalQueueIndex = childLeftIndex;
                    }
                    else
                    {
                        // right is even higher, move it up and continue
                        childRight.QueueIndex = finalQueueIndex;
                        _nodes[finalQueueIndex] = childRight;
                        finalQueueIndex = childRightIndex;
                    }
                }
                // Not swapping with left-child, does right-child exist?
                else if (childRightIndex > _numNodes)
                {
                    node.QueueIndex = finalQueueIndex;
                    _nodes[finalQueueIndex] = node;
                    break;
                }
                else
                {
                    // Check if the right-child is higher-priority than the current node
                    TItem childRight = _nodes[childRightIndex];
                    if (HasHigherPriority(childRight, node))
                    {
                        childRight.QueueIndex = finalQueueIndex;
                        _nodes[finalQueueIndex] = childRight;
                        finalQueueIndex = childRightIndex;
                    }
                    // Neither child is higher-priority than current, so finish and stop.
                    else
                    {
                        node.QueueIndex = finalQueueIndex;
                        _nodes[finalQueueIndex] = node;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Returns true if 'higher' has higher priority than 'lower', false otherwise.
        /// Note that calling HasHigherPriority(node, node) (ie. both arguments the same node) will return false
        /// </summary>
#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        private bool HasHigherPriority(TItem higher, TItem lower)
        {
            var cmp = _comparer(higher.Priority, lower.Priority);
            return (cmp < 0 || (cmp == 0 && higher.InsertionIndex < lower.InsertionIndex));
        }

        /// <summary>
        /// Removes the head of the queue (node with minimum priority; ties are broken by order of insertion), and returns it.
        /// If queue is empty, result is undefined
        /// O(log n)
        /// </summary>
#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public TItem Dequeue()
        {
#if DEBUG
            if (_numNodes <= 0)
            {
                throw new InvalidOperationException("Cannot call Dequeue() on an empty queue");
            }

            if (!IsValidQueue())
            {
                throw new InvalidOperationException("Queue has been corrupted (Did you update a node priority manually instead of calling UpdatePriority()?" +
                                                    "Or add the same node to two different queues?)");
            }
#endif

            TItem returnMe = _nodes[1];
            //If the node is already the last node, we can remove it immediately
            if (_numNodes == 1)
            {
                _nodes[1] = null;
                _numNodes = 0;
                return returnMe;
            }

            //Swap the node with the last node
            TItem formerLastNode = _nodes[_numNodes];
            _nodes[1] = formerLastNode;
            formerLastNode.QueueIndex = 1;
            _nodes[_numNodes] = null;
            _numNodes--;

            //Now bubble formerLastNode (which is no longer the last node) down
            CascadeDown(formerLastNode);
            return returnMe;
        }

        /// <summary>
        /// Resize the queue so it can accept more nodes.  All currently enqueued nodes are remain.
        /// Attempting to decrease the queue size to a size too small to hold the existing nodes results in undefined behavior
        /// O(n)
        /// </summary>
        public void Resize(int maxNodes)
        {
#if DEBUG
            if (maxNodes <= 0)
            {
                throw new InvalidOperationException("Queue size cannot be smaller than 1");
            }

            if (maxNodes < _numNodes)
            {
                throw new InvalidOperationException("Called Resize(" + maxNodes + "), but current queue contains " + _numNodes + " nodes");
            }
#endif

            TItem[] newArray = new TItem[maxNodes + 1];
            int highestIndexToCopy = Math.Min(maxNodes, _numNodes);
            Array.Copy(_nodes, newArray, highestIndexToCopy + 1);
            _nodes = newArray;
        }

        /// <summary>
        /// Returns the head of the queue, without removing it (use Dequeue() for that).
        /// If the queue is empty, behavior is undefined.
        /// O(1)
        /// </summary>
        public TItem First
        {
            get
            {
#if DEBUG
                if (_numNodes <= 0)
                {
                    throw new InvalidOperationException("Cannot call .First on an empty queue");
                }
#endif

                return _nodes[1];
            }
        }

        /// <summary>
        /// This method must be called on a node every time its priority changes while it is in the queue.  
        /// <b>Forgetting to call this method will result in a corrupted queue!</b>
        /// Calling this method on a node not in the queue results in undefined behavior
        /// O(log n)
        /// </summary>
#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void UpdatePriority(TItem node, TPriority priority)
        {
#if DEBUG
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }
            if (!Contains(node))
            {
                throw new InvalidOperationException("Cannot call UpdatePriority() on a node which is not enqueued: " + node);
            }
#endif

            node.Priority = priority;
            OnNodeUpdated(node);
        }

#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        private void OnNodeUpdated(TItem node)
        {
            //Bubble the updated node up or down as appropriate
            int parentIndex = node.QueueIndex >> 1;

            if (parentIndex > 0 && HasHigherPriority(node, _nodes[parentIndex]))
            {
                CascadeUp(node);
            }
            else
            {
                //Note that CascadeDown will be called if parentNode == node (that is, node is the root)
                CascadeDown(node);
            }
        }

        /// <summary>
        /// Removes a node from the queue.  The node does not need to be the head of the queue.  
        /// If the node is not in the queue, the result is undefined.  If unsure, check Contains() first
        /// O(log n)
        /// </summary>
#if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Remove(TItem node)
        {
#if DEBUG
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }
            if (!Contains(node))
            {
                throw new InvalidOperationException("Cannot call Remove() on a node which is not enqueued: " + node);
            }
#endif

            //If the node is already the last node, we can remove it immediately
            if (node.QueueIndex == _numNodes)
            {
                _nodes[_numNodes] = null;
                _numNodes--;
                return;
            }

            //Swap the node with the last node
            TItem formerLastNode = _nodes[_numNodes];
            _nodes[node.QueueIndex] = formerLastNode;
            formerLastNode.QueueIndex = node.QueueIndex;
            _nodes[_numNodes] = null;
            _numNodes--;

            //Now bubble formerLastNode (which is no longer the last node) up or down as appropriate
            OnNodeUpdated(formerLastNode);
        }

        public IEnumerator<TItem> GetEnumerator()
        {
#if NET_VERSION_4_5 // ArraySegment does not implement IEnumerable before 4.5
            IEnumerable<TItem> e = new ArraySegment<TItem>(_nodes, 1, _numNodes);
            return e.GetEnumerator();
#else
            for (int i = 1; i <= _numNodes; i++)
                yield return _nodes[i];
#endif
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// <b>Should not be called in production code.</b>
        /// Checks to make sure the queue is still in a valid state.  Used for testing/debugging the queue.
        /// </summary>
        public bool IsValidQueue()
        {
            for (int i = 1; i < _nodes.Length; i++)
            {
                if (_nodes[i] != null)
                {
                    int childLeftIndex = 2 * i;
                    if (childLeftIndex < _nodes.Length && _nodes[childLeftIndex] != null && HasHigherPriority(_nodes[childLeftIndex], _nodes[i]))
                        return false;

                    int childRightIndex = childLeftIndex + 1;
                    if (childRightIndex < _nodes.Length && _nodes[childRightIndex] != null && HasHigherPriority(_nodes[childRightIndex], _nodes[i]))
                        return false;
                }
            }
            return true;
        }
    }

    /// <summary>
    /// A simplified priority queue implementation.  Is stable, auto-resizes, and thread-safe, at the cost of being slightly slower than
    /// FastPriorityQueue
    /// Methods tagged as O(1) or O(log n) are assuming there are no duplicates.  Duplicates may increase the algorithmic complexity.
    /// </summary>
    /// <typeparam name="TItem">The type to enqueue</typeparam>
    /// <typeparam name="TPriority">The priority-type to use for nodes.  Must extend IComparable&lt;TPriority&gt;</typeparam>
    public class SimplePriorityQueue<TItem, TPriority> : IPriorityQueue<TItem, TPriority>
        where TPriority : IComparable<TPriority>
    {
        private class SimpleNode : GenericPriorityQueueNode<TPriority>
        {
            public TItem Data { get; private set; }

            public SimpleNode(TItem data)
            {
                Data = data;
            }
        }

        private const int INITIAL_QUEUE_SIZE = 10;
        private readonly GenericPriorityQueue<SimpleNode, TPriority> _queue;
        private readonly Dictionary<TItem, IList<SimpleNode>> _itemToNodesCache;
        private readonly IList<SimpleNode> _nullNodesCache;

        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        public SimplePriorityQueue() : this(Comparer<TPriority>.Default) { }

        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        /// <param name="comparer">The comparer used to compare TPriority values.  Defaults to Comparer&lt;TPriority&gt;.default</param>
        public SimplePriorityQueue(IComparer<TPriority> comparer) : this(comparer.Compare) { }

        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        /// <param name="comparer">The comparison function to use to compare TPriority values</param>
        public SimplePriorityQueue(Comparison<TPriority> comparer)
        {
            _queue = new GenericPriorityQueue<SimpleNode, TPriority>(INITIAL_QUEUE_SIZE, comparer);
            _itemToNodesCache = new Dictionary<TItem, IList<SimpleNode>>();
            _nullNodesCache = new List<SimpleNode>();
        }

        /// <summary>
        /// Given an item of type T, returns the exist SimpleNode in the queue
        /// </summary>
        private SimpleNode GetExistingNode(TItem item)
        {
            if (item == null)
            {
                return _nullNodesCache.Count > 0 ? _nullNodesCache[0] : null;
            }

            IList<SimpleNode> nodes;
            if (!_itemToNodesCache.TryGetValue(item, out nodes))
            {
                return null;
            }
            return nodes[0];
        }

        /// <summary>
        /// Adds an item to the Node-cache to allow for many methods to be O(1) or O(log n)
        /// </summary>
        private void AddToNodeCache(SimpleNode node)
        {
            if (node.Data == null)
            {
                _nullNodesCache.Add(node);
                return;
            }

            IList<SimpleNode> nodes;
            if (!_itemToNodesCache.TryGetValue(node.Data, out nodes))
            {
                nodes = new List<SimpleNode>();
                _itemToNodesCache[node.Data] = nodes;
            }
            nodes.Add(node);
        }

        /// <summary>
        /// Removes an item to the Node-cache to allow for many methods to be O(1) or O(log n) (assuming no duplicates)
        /// </summary>
        private void RemoveFromNodeCache(SimpleNode node)
        {
            if (node.Data == null)
            {
                _nullNodesCache.Remove(node);
                return;
            }

            IList<SimpleNode> nodes;
            if (!_itemToNodesCache.TryGetValue(node.Data, out nodes))
            {
                return;
            }
            nodes.Remove(node);
            if (nodes.Count == 0)
            {
                _itemToNodesCache.Remove(node.Data);
            }
        }

        /// <summary>
        /// Returns the number of nodes in the queue.
        /// O(1)
        /// </summary>
        public int Count
        {
            get
            {
                lock (_queue)
                {
                    return _queue.Count;
                }
            }
        }


        /// <summary>
        /// Returns the head of the queue, without removing it (use Dequeue() for that).
        /// Throws an exception when the queue is empty.
        /// O(1)
        /// </summary>
        public TItem First
        {
            get
            {
                lock (_queue)
                {
                    if (_queue.Count <= 0)
                    {
                        throw new InvalidOperationException("Cannot call .First on an empty queue");
                    }

                    return _queue.First.Data;
                }
            }
        }

        /// <summary>
        /// Removes every node from the queue.
        /// O(n)
        /// </summary>
        public void Clear()
        {
            lock (_queue)
            {
                _queue.Clear();
                _itemToNodesCache.Clear();
                _nullNodesCache.Clear();
            }
        }

        /// <summary>
        /// Returns whether the given item is in the queue.
        /// O(1)
        /// </summary>
        public bool Contains(TItem item)
        {
            lock (_queue)
            {
                if (item == null)
                {
                    return _nullNodesCache.Count > 0;
                }
                return _itemToNodesCache.ContainsKey(item);
            }
        }

        /// <summary>
        /// Removes the head of the queue (node with minimum priority; ties are broken by order of insertion), and returns it.
        /// If queue is empty, throws an exception
        /// O(log n)
        /// </summary>
        public TItem Dequeue()
        {
            lock (_queue)
            {
                if (_queue.Count <= 0)
                {
                    throw new InvalidOperationException("Cannot call Dequeue() on an empty queue");
                }

                SimpleNode node = _queue.Dequeue();
                RemoveFromNodeCache(node);
                return node.Data;
            }
        }

        /// <summary>
        /// Enqueue the item with the given priority, without calling lock(_queue) or AddToNodeCache(node)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        private SimpleNode EnqueueNoLockOrCache(TItem item, TPriority priority)
        {
            SimpleNode node = new SimpleNode(item);
            if (_queue.Count == _queue.MaxSize)
            {
                _queue.Resize(_queue.MaxSize * 2 + 1);
            }
            _queue.Enqueue(node, priority);
            return node;
        }

        /// <summary>
        /// Enqueue a node to the priority queue.  Lower values are placed in front. Ties are broken by first-in-first-out.
        /// This queue automatically resizes itself, so there's no concern of the queue becoming 'full'.
        /// Duplicates and null-values are allowed.
        /// O(log n)
        /// </summary>
        public void Enqueue(TItem item, TPriority priority)
        {
            lock (_queue)
            {
                IList<SimpleNode> nodes;
                if (item == null)
                {
                    nodes = _nullNodesCache;
                }
                else if (!_itemToNodesCache.TryGetValue(item, out nodes))
                {
                    nodes = new List<SimpleNode>();
                    _itemToNodesCache[item] = nodes;
                }
                SimpleNode node = EnqueueNoLockOrCache(item, priority);
                nodes.Add(node);
            }
        }

        /// <summary>
        /// Enqueue a node to the priority queue if it doesn't already exist.  Lower values are placed in front. Ties are broken by first-in-first-out.
        /// This queue automatically resizes itself, so there's no concern of the queue becoming 'full'.  Null values are allowed.
        /// Returns true if the node was successfully enqueued; false if it already exists.
        /// O(log n)
        /// </summary>
        public bool EnqueueWithoutDuplicates(TItem item, TPriority priority)
        {
            lock (_queue)
            {
                IList<SimpleNode> nodes;
                if (item == null)
                {
                    if (_nullNodesCache.Count > 0)
                    {
                        return false;
                    }
                    nodes = _nullNodesCache;
                }
                else if (_itemToNodesCache.ContainsKey(item))
                {
                    return false;
                }
                else
                {
                    nodes = new List<SimpleNode>();
                    _itemToNodesCache[item] = nodes;
                }
                SimpleNode node = EnqueueNoLockOrCache(item, priority);
                nodes.Add(node);
                return true;
            }
        }

        /// <summary>
        /// Removes an item from the queue.  The item does not need to be the head of the queue.  
        /// If the item is not in the queue, an exception is thrown.  If unsure, check Contains() first.
        /// If multiple copies of the item are enqueued, only the first one is removed.
        /// O(log n)
        /// </summary>
        public void Remove(TItem item)
        {
            lock (_queue)
            {
                SimpleNode removeMe;
                IList<SimpleNode> nodes;
                if (item == null)
                {
                    if (_nullNodesCache.Count == 0)
                    {
                        throw new InvalidOperationException("Cannot call Remove() on a node which is not enqueued: " + item);
                    }
                    removeMe = _nullNodesCache[0];
                    nodes = _nullNodesCache;
                }
                else
                {
                    if (!_itemToNodesCache.TryGetValue(item, out nodes))
                    {
                        throw new InvalidOperationException("Cannot call Remove() on a node which is not enqueued: " + item);
                    }
                    removeMe = nodes[0];
                    if (nodes.Count == 1)
                    {
                        _itemToNodesCache.Remove(item);
                    }
                }
                _queue.Remove(removeMe);
                nodes.Remove(removeMe);
            }
        }

        /// <summary>
        /// Call this method to change the priority of an item.
        /// Calling this method on a item not in the queue will throw an exception.
        /// If the item is enqueued multiple times, only the first one will be updated.
        /// (If your requirements are complex enough that you need to enqueue the same item multiple times <i>and</i> be able
        /// to update all of them, please wrap your items in a wrapper class so they can be distinguished).
        /// O(log n)
        /// </summary>
        public void UpdatePriority(TItem item, TPriority priority)
        {
            lock (_queue)
            {
                SimpleNode updateMe = GetExistingNode(item);
                if (updateMe == null)
                {
                    throw new InvalidOperationException("Cannot call UpdatePriority() on a node which is not enqueued: " + item);
                }
                _queue.UpdatePriority(updateMe, priority);
            }
        }

        /// <summary>
        /// Returns the priority of the given item.
        /// Calling this method on a item not in the queue will throw an exception.
        /// If the item is enqueued multiple times, only the priority of the first will be returned.
        /// (If your requirements are complex enough that you need to enqueue the same item multiple times <i>and</i> be able
        /// to query all their priorities, please wrap your items in a wrapper class so they can be distinguished).
        /// O(1)
        /// </summary>
        public TPriority GetPriority(TItem item)
        {
            lock (_queue)
            {
                SimpleNode findMe = GetExistingNode(item);
                if (findMe == null)
                {
                    throw new InvalidOperationException("Cannot call GetPriority() on a node which is not enqueued: " + item);
                }
                return findMe.Priority;
            }
        }

        #region Try* methods for multithreading
        /// Get the head of the queue, without removing it (use TryDequeue() for that).
        /// Useful for multi-threading, where the queue may become empty between calls to Contains() and First
        /// Returns true if successful, false otherwise
        /// O(1)
        public bool TryFirst(out TItem first)
        {
            lock (_queue)
            {
                if (_queue.Count <= 0)
                {
                    first = default(TItem);
                    return false;
                }

                first = _queue.First.Data;
                return true;
            }
        }

        /// <summary>
        /// Removes the head of the queue (node with minimum priority; ties are broken by order of insertion), and sets it to first.
        /// Useful for multi-threading, where the queue may become empty between calls to Contains() and Dequeue()
        /// Returns true if successful; false if queue was empty
        /// O(log n)
        /// </summary>
        public bool TryDequeue(out TItem first)
        {
            lock (_queue)
            {
                if (_queue.Count <= 0)
                {
                    first = default(TItem);
                    return false;
                }

                SimpleNode node = _queue.Dequeue();
                first = node.Data;
                RemoveFromNodeCache(node);
                return true;
            }
        }

        /// <summary>
        /// Attempts to remove an item from the queue.  The item does not need to be the head of the queue.  
        /// Useful for multi-threading, where the queue may become empty between calls to Contains() and Remove()
        /// Returns true if the item was successfully removed, false if it wasn't in the queue.
        /// If multiple copies of the item are enqueued, only the first one is removed.
        /// O(log n)
        /// </summary>
        public bool TryRemove(TItem item)
        {
            lock (_queue)
            {
                SimpleNode removeMe;
                IList<SimpleNode> nodes;
                if (item == null)
                {
                    if (_nullNodesCache.Count == 0)
                    {
                        return false;
                    }
                    removeMe = _nullNodesCache[0];
                    nodes = _nullNodesCache;
                }
                else
                {
                    if (!_itemToNodesCache.TryGetValue(item, out nodes))
                    {
                        return false;
                    }
                    removeMe = nodes[0];
                    if (nodes.Count == 1)
                    {
                        _itemToNodesCache.Remove(item);
                    }
                }
                _queue.Remove(removeMe);
                nodes.Remove(removeMe);
                return true;
            }
        }

        /// <summary>
        /// Call this method to change the priority of an item.
        /// Useful for multi-threading, where the queue may become empty between calls to Contains() and UpdatePriority()
        /// If the item is enqueued multiple times, only the first one will be updated.
        /// (If your requirements are complex enough that you need to enqueue the same item multiple times <i>and</i> be able
        /// to update all of them, please wrap your items in a wrapper class so they can be distinguished).
        /// Returns true if the item priority was updated, false otherwise.
        /// O(log n)
        /// </summary>
        public bool TryUpdatePriority(TItem item, TPriority priority)
        {
            lock (_queue)
            {
                SimpleNode updateMe = GetExistingNode(item);
                if (updateMe == null)
                {
                    return false;
                }
                _queue.UpdatePriority(updateMe, priority);
                return true;
            }
        }

        /// <summary>
        /// Attempt to get the priority of the given item.
        /// Useful for multi-threading, where the queue may become empty between calls to Contains() and GetPriority()
        /// If the item is enqueued multiple times, only the priority of the first will be returned.
        /// (If your requirements are complex enough that you need to enqueue the same item multiple times <i>and</i> be able
        /// to query all their priorities, please wrap your items in a wrapper class so they can be distinguished).
        /// Returns true if the item was found in the queue, false otherwise
        /// O(1)
        /// </summary>
        public bool TryGetPriority(TItem item, out TPriority priority)
        {
            lock (_queue)
            {
                SimpleNode findMe = GetExistingNode(item);
                if (findMe == null)
                {
                    priority = default(TPriority);
                    return false;
                }
                priority = findMe.Priority;
                return true;
            }
        }
        #endregion

        public IEnumerator<TItem> GetEnumerator()
        {
            List<TItem> queueData = new List<TItem>();
            lock (_queue)
            {
                //Copy to a separate list because we don't want to 'yield return' inside a lock
                foreach (var node in _queue)
                {
                    queueData.Add(node.Data);
                }
            }

            return queueData.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool IsValidQueue()
        {
            lock (_queue)
            {
                // Check all items in cache are in the queue
                foreach (IList<SimpleNode> nodes in _itemToNodesCache.Values)
                {
                    foreach (SimpleNode node in nodes)
                    {
                        if (!_queue.Contains(node))
                        {
                            return false;
                        }
                    }
                }

                // Check all items in queue are in cache
                foreach (SimpleNode node in _queue)
                {
                    if (GetExistingNode(node.Data) == null)
                    {
                        return false;
                    }
                }

                // Check queue structure itself
                return _queue.IsValidQueue();
            }
        }
    }

    /// <summary>
    /// A simplified priority queue implementation.  Is stable, auto-resizes, and thread-safe, at the cost of being slightly slower than
    /// FastPriorityQueue
    /// This class is kept here for backwards compatibility.  It's recommended you use SimplePriorityQueue&lt;TItem, TPriority&gt;
    /// </summary>
    /// <typeparam name="TItem">The type to enqueue</typeparam>
    public class SimplePriorityQueue<TItem> : SimplePriorityQueue<TItem, float>
    {
        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        public SimplePriorityQueue() { }

        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        /// <param name="comparer">The comparer used to compare priority values.  Defaults to Comparer&lt;float&gt;.default</param>
        public SimplePriorityQueue(IComparer<float> comparer) : base(comparer) { }

        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        /// <param name="comparer">The comparison function to use to compare priority values</param>
        public SimplePriorityQueue(Comparison<float> comparer) : base(comparer) { }
    }
    #endregion  
}