﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{
    class ViMaNakupujici:  Nakupujici
    {
        char[,] mapa;
        Souradnice target;
        int[,] obarveni = new int[5,5];
        bool isNabidkaFound=true;
        Souradnice randGlob;
        public static int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }
        int whatKolo=0;
        override public Smer Krok(char[,] mapa, int penize, Souradnice souradnice)
        {
            whatKolo = whatKolo + 1;
            this.mapa = mapa;
            Smer pohniSe;
            bool s = FindBestNabidka();
            
            if (!s)
            {


                //pohniSe = MoveRandom();
                //return (pohniSe);


                if (isNabidkaFound)
                {
                    randGlob.X = nahoda.Next(60)+10;
                    randGlob.Y = nahoda.Next(16)+1;
                    isNabidkaFound=false;
                }
                target.X = Clamp(-souradnice.X+randGlob.X,0,4);
                target.Y = Clamp(-souradnice.Y+randGlob.Y,0,4);
                //Console.WriteLine("Vektor: " + (target.X - 2) + " " + (target.Y - 2));
                while (obarveni[target.X,target.Y]==-1|| (target.X==2 && target.Y==2) ){
                    randGlob.X = nahoda.Next(60)+10;
                    randGlob.Y = nahoda.Next(16)+2;
                    target.X = Clamp(-souradnice.X+randGlob.X,0,4);
                    target.Y = Clamp(-souradnice.Y+randGlob.Y,0,4);
                }

                //Console.WriteLine("Vektor: " + (target.X - 2) + " " + (target.Y - 2));
                
            }
            else
            {
                isNabidkaFound=true;
            }
            //Console.WriteLine("target: " + target.X + " " + target.Y);
            FindPath(new Souradnice(2, 2), 0);
            /*for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(obarveni[j, i] + "(" + j +","+ i + ")");
                }
                Console.Write("\n");
            }

            //pohniSe = MoveRandom();*/
            pohniSe = FindNextSmer();
            //Console.WriteLine("pohni se: "+ pohniSe.X + " " + pohniSe.Y);
            //Console.WriteLine(obarveni[pohniSe.X + 2, pohniSe.Y + 2]);
            if (obarveni[pohniSe.X + 2, pohniSe.Y + 2] == -1)
            {
               // Console.WriteLine(obarveni[pohniSe.X + 2, pohniSe.Y + 2]);
                pohniSe = MoveRandom();
                randGlob.X = 0;
                randGlob.Y = 10;
            }

              

            return (pohniSe);
        }

        private Smer FindNextSmer()
        {/*
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(obarveni[j, i] + "(" + i + "," + j + ")");
                }
                Console.Write("\n");
            }
            int[,] docas = new int[5, 5];
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    docas[i, j] = obarveni[j, i];
                }
            }
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    obarveni[i, j] = docas[i, j];
                }
            }
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(obarveni[i,j] + "(" + i + "," + j + ")");
                }
                Console.Write("\n");
            }
            Console.WriteLine(obarveni[target.X, target.Y] + "(" + target.X + "," + target.Y + ")");*/


            
            //while (obarveni[target.X,target.Y] != 1)
            
                for (int i = obarveni[target.X, target.Y];i>0;i--)
                {
                    for (int k = -1; k < 2; k++)
                    {
                        for (int l = -1; l < 2; l++)
                            {
                        //Console.WriteLine("target: " + (target.X+k) + " " + (target.Y+l));

                        if (k + target.X >= 0 && k + target.X < 5 && target.Y + l >= 0 && target.Y + l < 5)
                                 {
                            if (obarveni[target.X,target.Y]-1 == obarveni[k + target.X,target.Y + l] && obarveni[k + target.X, target.Y + l] != -1 && (k + target.X) != 2 && (l + target.Y) != 2)
                                        {
                                            target.X = k + target.X;
                                            target.Y = target.Y + l;
                                //Console.WriteLine("target zmenen: " + (target.X ) + " " + (target.Y ));

                            }
                                        
                                            
                                 }

                            
                            }
                   // Console.Write("\n");
                    }
                    
                }
            

            /*for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(obarveni[i, j] + "(" + i + "," + j + ")");
                }
                Console.Write("\n");
            }*/
            //Console.WriteLine(obarveni[target.X, target.Y] + "(" + target.X + "," + target.Y + ")");
            while (obarveni[target.Y, target.X]==-1)
            {
                target.X = nahoda.Next(4);
                target.Y = nahoda.Next(4);
                
            }
            
            return new Smer((sbyte)((target.X - 2)),(sbyte)((target.Y - 2)));
            
        }

        private void FindPath(Souradnice meziBod, int vzdalenost)
        {
            vzdalenost++;
            for (int i = -1; i < 2; i++)
            
                for (int j = -1; j < 2; j++)
                
                    if (i + meziBod.X >= 0 && i + meziBod.X < 5 && j + meziBod.Y >= 0 && j + meziBod.Y < 5)
                    
                        if(obarveni[i + meziBod.X, j + meziBod.Y] > vzdalenost)
                        {
                            obarveni[i + meziBod.X, j + meziBod.Y] = vzdalenost;
                            FindPath(new Souradnice(i + meziBod.X, j + meziBod.Y), vzdalenost);
                        }
            

        }

        private bool FindBestNabidka()
        {
            bool isNabidka = false;
            int minNabidka = 10;
            if (whatKolo > 150)
                minNabidka = 10;
            for (int i = 0; i < 5; i++)
            {
                for( int j = 0; j < 5; j++)
                {
                    if (mapa[j, i] - 48 < minNabidka && mapa[j, i] > 48 && mapa[j, i] < 58)
                    {
                        minNabidka = mapa[j, i] - 48;
                        target.X = j;
                        target.Y = i;
                        isNabidka = true;
                    }

                    if (mapa[i, j] == 'X')
                    {
                        obarveni[i, j] = -1;
                    }
                    else
                        obarveni[i, j] = 16;
                }
            }
            obarveni[2,2] = 0;
            return isNabidka;
        }

        private Smer MoveRandom()
        {
            return new Smer((sbyte)(nahoda.Next(3) - 1), (sbyte)(nahoda.Next(3) - 1));
        }
    }
}
