﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Na�� prioritou je se nenechat chytit zlod�jem, a proto p�ed n�m ut�k�me. 
 * Kdy� n�s nikdo nehon�, tak hled�me nab�dky v cel�m v��ezu a nejv�t�� prioritu m� 
 * vzd�lenost a n�sledn� pot� cena tzn. pokud jsou dv� nab�dky stejn� daleko, beru tu 
 * levn�j�� a a� pot� tu dra���. Kdy� nenajdeme nab�dku ani n�s nikdo nehon� chod�me do 
 * "obd�ln�ku", abychom pro�li co nejv�t�� plochu. Podrobn�j�� koment�� je p��mo v k�du u 
 * jednotliv�ch metod.  
 */





namespace SablonaNakup
{
    class DivnyNakupujici : Nakupujici
    {
        private struct InfoZlodej
        {
            public Souradnice PoziceZlodeje;
            public char Kdo;
        }
        private struct Nabidka
        {
            public Souradnice PoziceNabidky;
            public int hodnota;
        }
        private struct NabidkaStres
        {
            public int X;
            public int Y;
            public int hodnota;
        }
        private Souradnice dveKola;
        private Souradnice jednoKolo;
        private int counterkol = 0;
        private Random rnd = new Random();
        private int penizenakoncikola;
        public DivnyNakupujici()
        {
        }
        public override Smer Krok(char[,] castMapy, int penize, Souradnice souradnice)
        {
            counterkol++;
            //osetreni situace, kde by se hrac zasekl na jednom miste nebo by chodil v loopu 2 kroku
            if ((counterkol > 2) && (dveKola.X == souradnice.X && dveKola.Y == souradnice.Y))
            {
                return new Smer((sbyte)(nahoda.Next(3) - 1), (sbyte)(nahoda.Next(3) - 1));
            }
            if (counterkol == 1)
            {
                jednoKolo.X = souradnice.X;
                jednoKolo.Y = souradnice.Y;
            }
            if (counterkol > 1)
            {
                dveKola = jednoKolo;
                jednoKolo.X = souradnice.X;
                jednoKolo.Y = souradnice.Y;
            }
            //nabidky na ktere nemam dost penez se jevi jako zdi
            if (penize < 10)
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (castMapy[i, j] > 48 && castMapy[i, j] < 58)
                        {
                            if (castMapy[i, j] - 48 > penize)
                            {
                                castMapy[i, j] = 'X';
                            }
                        }
                    }
                }
            }
            Souradnice pozicenabidky;
            InfoZlodej zlodej = NajdiZlodeje(castMapy);//najde nejvzdalenejsiho zlodeje na ktereho vidime a podle toho vybira cestu
            int[,] minimapa = new int[5, 5];//interni mapa pro overeni mozne pozice zlodeje po 1 kole
            int[,] vektorypohybu = { { 1, 0 }, { 1, -1 }, { 0, -1 }, { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, 1 }, { 1, 1 } };//ruzne moznosti pohybu v poradi proti smeru hodinovych rucicek
            if (zlodej.PoziceZlodeje.X == 2 && zlodej.PoziceZlodeje.Y == 2)//nenasli jsme zadneho zlodeje
            {
                pozicenabidky = NajdiNabidkuKlid(castMapy);//najde nejblizsi(popr. nejlevnejsi nabidku)
                if (pozicenabidky.X == 2 && pozicenabidky.Y == 2)//nenasli jsme zadnou nabidku
                {
                    //pokudli jsme na kraji mapy, snazime se dostat od nej
                    if (souradnice.Y > 0 && souradnice.Y < 19 && souradnice.X == 0)//levy okraj
                    {
                        if (castMapy[3, 1] == ' ')
                        {
                            return new Smer(1, -1);
                        }
                        if (castMapy[3, 2] == ' ')
                        {
                            return new Smer(1, 0);
                        }
                        if (castMapy[3, 3] == ' ')
                        {
                            return new Smer(1, 1);
                        }
                    }
                    if (souradnice.X > 0 && souradnice.X < 79 && souradnice.Y == 0)//horni okraj
                    {
                        if (castMapy[3, 3] == ' ')
                        {
                            return new Smer(1, 1);
                        }
                        if (castMapy[2, 3] == ' ')
                        {
                            return new Smer(0, 1);
                        }
                        if (castMapy[1, 3] == ' ')
                        {
                            return new Smer(-1, 1);
                        }
                    }
                    if (souradnice.Y > 0 && souradnice.Y < 19 && souradnice.X == 79)//pravy okraj
                    {
                        if (castMapy[1, 3] == ' ')
                        {
                            return new Smer(-1, 1);
                        }
                        if (castMapy[1, 2] == ' ')
                        {
                            return new Smer(-1, 0);
                        }
                        if (castMapy[1, 1] == ' ')
                        {
                            return new Smer(-1, -1);
                        }
                    }
                    if (souradnice.X > 0 && souradnice.X < 79 && souradnice.Y == 19)//dolni okraj
                    {
                        if (castMapy[1, 1] == ' ')
                        {
                            return new Smer(-1, -1);
                        }
                        if (castMapy[2, 1] == ' ')
                        {
                            return new Smer(0, -1);
                        }
                        if (castMapy[3, 1] == ' ')
                        {
                            return new Smer(1, -1);
                        }
                    }
                    if (souradnice.X == 0 && souradnice.Y == 0)//levy horni roh
                    {
                        if (castMapy[3, 3] == ' ')
                        {
                            return new Smer(1, 1);
                        }
                    }
                    if (souradnice.X == 79 && souradnice.Y == 0)//pravy horni roh
                    {
                        if (castMapy[1, 3] == ' ')
                        {
                            return new Smer(-1, 1);
                        }
                    }
                    if (souradnice.X == 79 && souradnice.Y == 19)//pravy dolni roh
                    {
                        if (castMapy[1, 1] == ' ')
                        {
                            return new Smer(-1, -1);
                        }
                    }
                    if (souradnice.X == 0 && souradnice.Y == 19)//levy dolni roh
                    {
                        if (castMapy[3, 1] == ' ')
                        {
                            return new Smer(1, -1);
                        }
                    }
                    //pokudli nejsme na krajich nebo uprostred, chodime po mape po pravotocivem "viru", ten je delen na 4 podoblasti, ktere maji nastaveny smer chozeni
                    //kdyz se je na ceste nejaka prekazka, snazim se ji obejit po ceste, ktera je dale od stredu (viz. for cykly)
                    if (souradnice.X < 60 && souradnice.Y < 8)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            if (castMapy[2 + vektorypohybu[i, 0], 2 + vektorypohybu[i, 1]] == ' ')
                            {
                                return new Smer((sbyte)vektorypohybu[i, 0], (sbyte)vektorypohybu[i, 1]);
                            }
                        }
                        return new Smer(0, 0);
                    }
                    if (souradnice.X < 20 && souradnice.Y >= 8)
                    {
                        for (int i = 2; i < 10; i++)
                        {
                            if (castMapy[2 + vektorypohybu[i % 8, 0], 2 + vektorypohybu[i % 8, 1]] == ' ')
                            {
                                return new Smer((sbyte)vektorypohybu[i % 8, 0], (sbyte)vektorypohybu[i % 8, 1]);
                            }
                        }
                        return new Smer(0, 0);
                    }
                    if (souradnice.X >= 20 && souradnice.Y >= 12)
                    {
                        for (int i = 4; i < 12; i++)
                        {
                            if (castMapy[2 + vektorypohybu[i % 8, 0], 2 + vektorypohybu[i % 8, 1]] == ' ')
                            {
                                return new Smer((sbyte)vektorypohybu[i % 8, 0], (sbyte)vektorypohybu[i % 8, 1]);
                            }
                        }
                        return new Smer(0, 0);
                    }
                    if (souradnice.X >= 60 && souradnice.Y < 12)
                    {
                        for (int i = 6; i < 14; i++)
                        {
                            if (castMapy[2 + vektorypohybu[i % 8, 0], 2 + vektorypohybu[i % 8, 1]] == ' ')
                            {
                                return new Smer((sbyte)vektorypohybu[i % 8, 0], (sbyte)vektorypohybu[i % 8, 1]);
                            }
                        }
                        return new Smer(0, 0);
                    }
                    if (souradnice.X < 60 && souradnice.X >= 20 && souradnice.Y < 12 && souradnice.Y >= 8)//chozeni kdyz jsme uprostred "viru"
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            if (castMapy[2 + vektorypohybu[i, 0], 2 + vektorypohybu[i, 1]] == ' ')//doplnit spravny znak pro prazdne misto
                            {
                                return new Smer((sbyte)vektorypohybu[i, 0], (sbyte)vektorypohybu[i, 1]);
                            }
                        }
                        return new Smer(0, 0);
                    }
                }
                else//nasli jsme nabidku
                {
                    if (pozicenabidky.X < 4 && pozicenabidky.X > 0 && pozicenabidky.Y < 4 && pozicenabidky.Y > 0)//nabidka je na dosah jednoho skoku, tak ji seberu
                    {
                        switch (pozicenabidky.X)
                        {
                            case 1 when pozicenabidky.Y == 1:
                                return new Smer(-1, -1);
                            case 1 when pozicenabidky.Y == 2:
                                return new Smer(-1, 0);
                            case 1 when pozicenabidky.Y == 3:
                                return new Smer(-1, 1);
                            case 2 when pozicenabidky.Y == 1:
                                return new Smer(0, -1);
                            case 2 when pozicenabidky.Y == 3:
                                return new Smer(0, 1);
                            case 3 when pozicenabidky.Y == 1:
                                return new Smer(1, -1);
                            case 3 when pozicenabidky.Y == 2:
                                return new Smer(1, 0);
                            case 3 when pozicenabidky.Y == 3:
                                return new Smer(1, 1);
                        }
                    }
                    else//zavola fci ktera vyhodnoti cestu pro vzdalenejsi nabidku
                    {
                        return VyhodnotCestuSeZdmi(castMapy, zlodej.PoziceZlodeje, pozicenabidky);
                    }
                }
            }
            else//nasli jsme zlodeje
            {
                pozicenabidky = NajdiNabidkuStres(zlodej.PoziceZlodeje, castMapy);//najde optimalni nabidku, kde bere v potaz nalezeneho zlodeje
                if (pozicenabidky.X == 2 && pozicenabidky.Y == 2)//nenasli jsme zadnou nabidku, timpadem vyberu jakykoliv smer, kde me nemuze zlodej okrast
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (zlodej.PoziceZlodeje.X + vektorypohybu[i, 0] > -1 && zlodej.PoziceZlodeje.X + vektorypohybu[i, 0] < 5 && zlodej.PoziceZlodeje.Y + vektorypohybu[i, 1] > -1 && zlodej.PoziceZlodeje.Y + vektorypohybu[i, 1] < 5)
                        {
                            minimapa[zlodej.PoziceZlodeje.X + vektorypohybu[i, 0], zlodej.PoziceZlodeje.Y + vektorypohybu[i, 1]] = 1;
                        }
                    }
                    for (int i = 0; i < 8; i++)
                    {
                        if (castMapy[2 + vektorypohybu[i, 0], 2 + vektorypohybu[i, 1]] == ' ' && minimapa[2 + vektorypohybu[i, 0], 2 + vektorypohybu[i, 1]] != 1)
                        {
                            return new Smer((sbyte)vektorypohybu[i, 0], (sbyte)vektorypohybu[i, 1]);
                        }
                    }
                    for (int i = 0; i < 8; i++)
                    {
                        if (castMapy[2 + vektorypohybu[i, 0], 2 + vektorypohybu[i, 1]] == ' ')
                        {
                            return new Smer((sbyte)vektorypohybu[i, 0], (sbyte)vektorypohybu[i, 1]);
                        }
                    }
                    return new Smer(0, 0);
                }
                else//nasli jsme nabidku
                {
                    if (pozicenabidky.X < 4 && pozicenabidky.X > 0 && pozicenabidky.Y < 4 && pozicenabidky.Y > 0)//nabidka je na dosah 1 kroku, tak ji seberu
                    {
                        switch (pozicenabidky.X)
                        {
                            case 1 when pozicenabidky.Y == 1:
                                return new Smer(-1, -1);
                            case 1 when pozicenabidky.Y == 2:
                                return new Smer(-1, 0);
                            case 1 when pozicenabidky.Y == 3:
                                return new Smer(-1, 1);
                            case 2 when pozicenabidky.Y == 1:
                                return new Smer(0, -1);
                            case 2 when pozicenabidky.Y == 3:
                                return new Smer(0, 1);
                            case 3 when pozicenabidky.Y == 1:
                                return new Smer(1, -1);
                            case 3 when pozicenabidky.Y == 2:
                                return new Smer(1, 0);
                            case 3 when pozicenabidky.Y == 3:
                                return new Smer(1, 1);
                        }
                    }
                    else//nabidka je ve vzdalenosti 2 kroku, timpadem zavolam fci, ktera vyhodnoti tuto cestu
                    {
                        return VyhodnotCestuSeZdmi(castMapy, zlodej.PoziceZlodeje, pozicenabidky);
                    }
                }
            }
            penizenakoncikola = penize;
            return new Smer(0, 0);
        }
        private InfoZlodej NajdiZlodeje(char[,] castMapy) //vraci souradnici zlodeje a zlodeje ktery ho honi
        {
            //najde nejvdalenejsiho zlodeje
            //pamatuje si kdo ho honil posledni
            Souradnice SouradniceZlodeje = new Souradnice();
            SouradniceZlodeje.X = 2;
            SouradniceZlodeje.Y = 2;

            // kdyz nenajdu zlodeje
            InfoZlodej Zlodej;
            Zlodej.PoziceZlodeje = SouradniceZlodeje;
            Zlodej.Kdo = '0';

            InfoZlodej[] vnejsi = new InfoZlodej[16];
            int KamMamZapisovatVnejsi = 0;
            InfoZlodej[] vnitrni = new InfoZlodej[8];
            int KamMamZapisovatVnitrni = 0;

            for (int y = 0; y < 5; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    if ((castMapy[x, y] >= 'A' && castMapy[x, y] < 'X') || (castMapy[x, y] > 'X' && castMapy[x, y] <= 'Z')) //hleda zlodeje 
                    {
                        if (((x == 0) && (y >= 0 && y <= 4)) || ((x == 4) && (y >= 0 && y <= 4)) || ((y == 0) && (x >= 0 && x <= 4)) || ((y == 4) && (x >= 0 && x <= 4)))  //najde-li zlodeje na dva kroky ode me
                        {
                            vnejsi[KamMamZapisovatVnejsi].PoziceZlodeje.X = x;
                            vnejsi[KamMamZapisovatVnejsi].PoziceZlodeje.Y = y;
                            vnitrni[KamMamZapisovatVnejsi].Kdo = castMapy[x, y];
                            KamMamZapisovatVnejsi++;
                        }
                        if (((x == 1) && (y >= 1 && y <= 3)) || ((x == 3) && (y >= 1 && y <= 3)) || ((y == 1) && (x == 2)) || ((y == 3) && (x == 2)))   //najde-li zlodeje na jeden krok ode me
                        {
                            vnitrni[KamMamZapisovatVnitrni].PoziceZlodeje.X = x;
                            vnitrni[KamMamZapisovatVnitrni].PoziceZlodeje.Y = y;
                            vnitrni[KamMamZapisovatVnejsi].Kdo = castMapy[x, y];
                            KamMamZapisovatVnitrni++;
                        }
                    }
                }
            }
            int CounterVnejsi = rnd.Next(KamMamZapisovatVnejsi);
            int CounterVnitrni = rnd.Next(KamMamZapisovatVnitrni);

            if (KamMamZapisovatVnejsi == 0 && KamMamZapisovatVnitrni == 0) // nic nenasel
            {
                return Zlodej;
            }
            if (KamMamZapisovatVnejsi > 0 && KamMamZapisovatVnitrni > 0) // kdyz najdu vic nabidek ve vnejsim i vnitrnim poli, tak beru tu blizsi
            {
                Zlodej.PoziceZlodeje.X = vnejsi[CounterVnejsi].PoziceZlodeje.X;
                Zlodej.PoziceZlodeje.Y = vnejsi[CounterVnejsi].PoziceZlodeje.Y;
                Zlodej.Kdo = vnejsi[CounterVnejsi].Kdo;
                return Zlodej;
            }
            if (KamMamZapisovatVnejsi == 0 && KamMamZapisovatVnitrni > 0) //najdu ve vnitrnim
            {
                Zlodej.PoziceZlodeje.X = vnitrni[CounterVnitrni].PoziceZlodeje.X;
                Zlodej.PoziceZlodeje.Y = vnitrni[CounterVnitrni].PoziceZlodeje.Y;
                Zlodej.Kdo = vnitrni[CounterVnitrni].Kdo;
                return Zlodej;
            }
            if (KamMamZapisovatVnejsi > 0 && KamMamZapisovatVnitrni == 0) // najdu ve vnejsim
            {
                Zlodej.PoziceZlodeje.X = vnejsi[CounterVnejsi].PoziceZlodeje.X;
                Zlodej.PoziceZlodeje.Y = vnejsi[CounterVnejsi].PoziceZlodeje.Y;
                Zlodej.Kdo = vnejsi[CounterVnejsi].Kdo;
                return Zlodej;
            }

            return Zlodej;

        }
        private Souradnice NajdiNabidkuKlid(char[,] castMapy)
        {
            //je volana kdyz se nenajde zlodej a hleda po cele plose vyrezu z mapy
            //dostava cast mapy
            //vraci souradnici nabidky
            //funguje stejne jako najdiZlodeje
            Souradnice SouradniceNabidky = new Souradnice();
            Nabidka[] vnejsi = new Nabidka[16];
            int KamMamZapisovatVnejsi = 0;
            Nabidka[] vnitrni = new Nabidka[8];
            int KamMamZapisovatVnitrni = 0;
            //kdyz nic nenajdu
            SouradniceNabidky.X = 2;
            SouradniceNabidky.Y = 2;

            for (int y = 0; y < 5; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    if (castMapy[x, y] >= '1' && castMapy[x, y] <= '9')
                    {
                        if (((x == 0) && (y >= 0 && y <= 4)) || ((x == 4) && (y >= 0 && y <= 4)) || ((y == 0) && (x >= 0 && x <= 4)) || ((y == 4) && (x >= 0 && x <= 4))) //nasel jsem nabidku na 2 kroky
                        {
                            vnejsi[KamMamZapisovatVnejsi].PoziceNabidky.X = x;
                            vnejsi[KamMamZapisovatVnejsi].PoziceNabidky.Y = y;
                            vnejsi[KamMamZapisovatVnejsi].hodnota = castMapy[x, y] - 48;
                            KamMamZapisovatVnejsi++;

                        }
                        if (((x == 1) && (y >= 1 && y <= 3)) || ((x == 3) && (y >= 1 && y <= 3)) || ((y == 1) && (x == 2)) || ((y == 3) && (x == 2)))  //nasel jsem nabidku na 1 krok
                        {
                            vnitrni[KamMamZapisovatVnitrni].PoziceNabidky.X = x;
                            vnitrni[KamMamZapisovatVnitrni].PoziceNabidky.Y = y;
                            vnitrni[KamMamZapisovatVnitrni].hodnota = castMapy[x, y] - 48;
                            KamMamZapisovatVnitrni++;
                        }
                    }
                }
            }

            if (KamMamZapisovatVnejsi == 0 && KamMamZapisovatVnitrni == 0) // nic nenasel
            {
                return SouradniceNabidky;
            }
            if ((KamMamZapisovatVnejsi > 0 && KamMamZapisovatVnitrni > 0) || (KamMamZapisovatVnejsi == 0 && KamMamZapisovatVnitrni > 0)) // kdyz najdu vic nabidek ve vnejsim i vnitrnim poli, tak beru tu blizsi a levnejsi
            {
                int min = vnitrni[0].hodnota;
                int index = 0;
                if (KamMamZapisovatVnitrni == 1)
                {
                    index = 0;
                }
                else
                {
                    for (int i = 1; i < KamMamZapisovatVnitrni; i++)
                    {
                        if (vnitrni[i].hodnota < min)
                        {
                            min = vnitrni[i].hodnota;
                            index = i;
                        }
                    }
                }
                SouradniceNabidky.X = vnitrni[index].PoziceNabidky.X;
                SouradniceNabidky.Y = vnitrni[index].PoziceNabidky.Y;
                return SouradniceNabidky;
            }


            if (KamMamZapisovatVnejsi > 0 && KamMamZapisovatVnitrni == 0) // najdu ve vnejsim beru tu nejlevnejsi
            {
                int min = vnejsi[0].hodnota;
                int index = 0;
                if (KamMamZapisovatVnejsi == 1)
                {
                    index = 0;
                }
                else
                {
                    for (int i = 1; i < KamMamZapisovatVnejsi; i++)
                    {
                        if (vnitrni[i].hodnota < min)
                        {
                            min = vnejsi[i].hodnota;
                            index = i;
                        }
                    }
                }
                SouradniceNabidky.X = vnejsi[index].PoziceNabidky.X;
                SouradniceNabidky.Y = vnejsi[index].PoziceNabidky.Y;
                return SouradniceNabidky;
            }
            return SouradniceNabidky;
        }
        private Souradnice NajdiNabidkuStres(Souradnice Zlodeje, char[,] castMapy)
        {

            //pocitame s tim ze nas zlodej chce chytit
            //vejir -> souradnice nabidky -> tak aby nakupujici vzdy odchazel od zlodeje
            int soumernost = -1; // budu podle toho delat stredovou soumernost vejire
            // vraci 2,2 kdy nenajde nabidku
            Souradnice NabidkaStres;
            NabidkaStres[] SeznamNabidekVnejsi = new NabidkaStres[12];
            NabidkaStres[] SeznamNabidekVnitrni = new NabidkaStres[12];

            NabidkaStres.X = 2;
            NabidkaStres.Y = 2;

            int counterNabidekDvaKroky = 0; // kolik jsem nalezl nabidek na 2 Kroky
            int counterNabidekJedenKrok = 0; // kolik jsem nalezl nabidek na 1 Krok

            if ((Zlodeje.X == 0 && Zlodeje.Y == 0) || (Zlodeje.X == 4 && Zlodeje.Y == 4) || (Zlodeje.X == 1 && Zlodeje.Y == 1) || (Zlodeje.X == 3 && Zlodeje.Y == 3)) // pro zlodeje na 0,0 a 4,4 a 1,1 a 3,3
            {
                if (Zlodeje.X == 0 || Zlodeje.X == 1)
                {
                    soumernost = 1;
                    int y;
                    for (y = 0; y < 5; y++)
                    {
                        if (castMapy[4, y] >= '1' && castMapy[4, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                    int x;
                    for (x = 1; x < 4; x++)
                    {
                        if (castMapy[x, 4] >= '1' && castMapy[x, 4] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 4] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                }

                if (Zlodeje.X == 4 || Zlodeje.X == 3)
                {
                    int y;
                    for (y = 1; y < 4; y++)
                    {
                        if (castMapy[0, y] >= '1' && castMapy[0, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                    int x;
                    for (x = 1; x < 4; x++)
                    {
                        if (castMapy[x, 0] >= '1' && castMapy[x, 0] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 0] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                }
                //kouka na povolena mista v mape a zapisuju je do prislusnzch poli podle poctu kroku
                //diagonalu maji vzdy stejnou -> vyuzivam soumernosti pro kontrolu povolenych mist
                if (castMapy[2 + (1 * soumernost), 2 + (1 * soumernost)] >= '1' && castMapy[2 + (1 * soumernost), 2 + (1 * soumernost)] <= '9') //kdyz najdu diagonalne ihned vracim vysledek
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 + (1 * soumernost), 2 + (1 * soumernost)] - 48;
                    counterNabidekJedenKrok++;
                }

                if (castMapy[4, 0] >= '1' && castMapy[4, 0] <= '9')
                {
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, 0] - 48;
                    counterNabidekDvaKroky++;
                }

                if (castMapy[0, 4] >= '1' && castMapy[0, 4] <= '9')
                {
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, 4] - 48;
                    counterNabidekDvaKroky++;
                }

                if (castMapy[3, 1] >= '1' && castMapy[3, 1] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 1] - 48;
                    counterNabidekJedenKrok++;
                }

                if (castMapy[1, 3] >= '1' && castMapy[1, 3] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 3] - 48;
                    counterNabidekJedenKrok++;
                }

                if (castMapy[2 + (1 * soumernost), 2] >= '1' && castMapy[2 + (1 * soumernost), 2] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 + (1 * soumernost), 2] - 48;
                    counterNabidekJedenKrok++;
                }

                if (castMapy[2, 2 + (1 * soumernost)] >= '1' && castMapy[2, 2 + (1 * soumernost)] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 2 + (1 * soumernost)] - 48;
                    counterNabidekJedenKrok++;
                }
            }

            if ((Zlodeje.X == 4 && Zlodeje.Y == 0) || (Zlodeje.X == 0 && Zlodeje.Y == 4) || (Zlodeje.X == 1 && Zlodeje.Y == 3) || (Zlodeje.X == 3 && Zlodeje.Y == 1)) // pro zlodeje na 4,0 a 0,4 a 1,3 a 3,1
            {                                                                             //uplne stejny princip jako if nad timto akorat rotovany o 90 stupnu
                if (Zlodeje.X == 4 || Zlodeje.X == 3)
                {
                    soumernost = 1;
                    int y;
                    for (y = 1; y < 4; y++)
                    {
                        if (castMapy[0, y] >= '1' && castMapy[0, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                    int x;
                    for (x = 1; x < 4; x++)
                    {
                        if (castMapy[x, 4] >= '1' && castMapy[x, 4] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 4] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                }

                if (Zlodeje.X == 0 || Zlodeje.X == 1)
                {
                    int y;
                    for (y = 0; y < 4; y++)
                    {
                        if (castMapy[4, y] >= '1' && castMapy[4, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                    int x;
                    for (x = 1; x < 4; x++)
                    {
                        if (castMapy[x, 0] >= '1' && castMapy[x, 0] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 0] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                }
                //kouka na povolena mista v mape a zapisuju je do prislusnzch poli podle poctu kroku
                //diagonalu maji vzdy stejnou -> vyuzivam soumernosti pro kontrolu povolenych mist

                if (castMapy[2 - (1 * soumernost), 2 + (1 * soumernost)] >= '1' && castMapy[2 - (1 * soumernost), 2 + (1 * soumernost)] <= '9') //kdyz najdu diagonalne ihned vracim vysledek
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 - (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 - (1 * soumernost), 2 + (1 * soumernost)] - 48;
                    counterNabidekJedenKrok++;
                }

                if (castMapy[0, 0] >= '1' && castMapy[0, 0] <= '9')
                {
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, 0] - 48;
                    counterNabidekDvaKroky++;
                }

                if (castMapy[1, 1] >= '1' && castMapy[1, 1] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 1] - 48;
                    counterNabidekJedenKrok++;
                }

                if (castMapy[3, 3] >= '1' && castMapy[3, 3] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 3] - 48;
                    counterNabidekJedenKrok++;
                }

                if (castMapy[4, 4] >= '1' && castMapy[4, 4] <= '9')
                {
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                    SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, 4] - 48;
                    counterNabidekDvaKroky++;
                }

                if (castMapy[2 - (1 * soumernost), 2] >= '1' && castMapy[2 - (1 * soumernost), 2] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 - (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 - (1 * soumernost), 2] - 48;
                    counterNabidekJedenKrok++;
                }

                if (castMapy[2, 2 - (1 * soumernost)] >= '1' && castMapy[2, 2 - (1 * soumernost)] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].Y = 2 - (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].X = 2;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 2 - (1 * soumernost)] - 48;
                    counterNabidekJedenKrok++;
                }
            }


            if ((Zlodeje.X == 1 && Zlodeje.Y == 0) || (Zlodeje.X == 3 && Zlodeje.Y == 4)) //Zlodej je na 1,0 nebo 3,4
            {
                if (Zlodeje.X == 1)
                {
                    if (castMapy[3, 3] >= '1' && castMapy[3, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[1, 3] >= '1' && castMapy[1, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 3] >= '1' && castMapy[2, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[3, 2] >= '1' && castMapy[3, 2] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 2] - 48;
                        counterNabidekJedenKrok++;
                    }

                    soumernost = -1;
                    int x;
                    for (x = 1; x < 5; x++)
                    {
                        if (castMapy[x, 4] >= '1' && castMapy[x, 4] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 4] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                    if (castMapy[4, 3] >= '1' && castMapy[4, 3] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, 3] - 48;
                        counterNabidekDvaKroky++;
                    }
                    if (castMapy[4, 2] >= '1' && castMapy[4, 2] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, 2] - 48;
                        counterNabidekDvaKroky++;
                    }

                }

                if (Zlodeje.X == 3)
                {
                    if (castMapy[1, 1] >= '1' && castMapy[1, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[1, 2] >= '1' && castMapy[1, 2] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 2] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 1] >= '1' && castMapy[2, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[3, 1] >= '1' && castMapy[3, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    int x;
                    for (x = 0; x < 4; x++)
                    {
                        if (castMapy[x, 0] >= '1' && castMapy[x, 0] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 0] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                    if (castMapy[0, 1] >= '1' && castMapy[0, 1] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, 1] - 48;
                        counterNabidekDvaKroky++;
                    }
                    if (castMapy[0, 2] >= '1' && castMapy[0, 2] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, 2] - 48;
                        counterNabidekDvaKroky++;
                    }
                }
            }

            if ((Zlodeje.X == 0 && Zlodeje.Y == 1) || (Zlodeje.X == 4 && Zlodeje.Y == 3)) //Zlodej je na 0,1 nebo 4,3
            {
                if (Zlodeje.X == 0)
                {
                    if (castMapy[3, 3] >= '1' && castMapy[3, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 3] >= '1' && castMapy[2, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[3, 1] >= '1' && castMapy[3, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[3, 2] >= '1' && castMapy[3, 2] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 2] - 48;
                        counterNabidekJedenKrok++;
                    }
                    soumernost = -1;
                    int y;
                    for (y = 1; y < 5; y++)
                    {
                        if (castMapy[4, y] >= '1' && castMapy[4, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                    if (castMapy[3, 4] >= '1' && castMapy[3, 4] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 3;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[3, 4] - 48;
                        counterNabidekDvaKroky++;
                    }
                    if (castMapy[2, 4] >= '1' && castMapy[2, 4] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 2;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[2, 4] - 48;
                        counterNabidekDvaKroky++;
                    }
                }

                if (Zlodeje.X == 4)
                {
                    if (castMapy[1, 1] >= '1' && castMapy[1, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[1, 3] >= '1' && castMapy[1, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[1, 2] >= '1' && castMapy[1, 2] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 2] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 1] >= '1' && castMapy[2, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 1] - 48;
                        counterNabidekJedenKrok++;
                    }

                    int y;
                    for (y = 0; y < 4; y++)
                    {
                        if (castMapy[0, y] >= '1' && castMapy[0, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                    if (castMapy[1, 0] >= '1' && castMapy[1, 0] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 1;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[1, 0] - 48;
                        counterNabidekDvaKroky++;
                    }
                    if (castMapy[2, 0] >= '1' && castMapy[2, 0] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 2;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[2, 0] - 48;
                        counterNabidekDvaKroky++;
                    }
                }
            }
            if ((Zlodeje.X == 2 && Zlodeje.Y == 0) || (Zlodeje.X == 2 && Zlodeje.Y == 4) || (Zlodeje.X == 2 && Zlodeje.Y == 3) || (Zlodeje.X == 2 && Zlodeje.Y == 1)) //Zlodej je na 2,0 nebo 2,4 a 2,1 a 2,3
            {
                if (Zlodeje.Y == 0 || Zlodeje.Y == 1)
                {
                    soumernost = 1;
                    for (int x = 0; x < 5; x++)
                    {
                        if (castMapy[x, 4] >= '1' && castMapy[x, 4] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 4] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                }

                if (Zlodeje.Y == 4 || Zlodeje.Y == 3)
                {
                    for (int x = 0; x < 5; x++)
                    {
                        if (castMapy[x, 0] >= '1' && castMapy[x, 0] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 0] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                }
                if (castMapy[2, 2 + (1 * soumernost)] >= '1' && castMapy[2, 2 + (1 * soumernost)] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 2 + (1 * soumernost)] - 48;
                    counterNabidekJedenKrok++;
                }
                if (castMapy[2 - (1 * soumernost), 2 + (1 * soumernost)] >= '1' && castMapy[2 - (1 * soumernost), 2 + (1 * soumernost)] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 - (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 - (1 * soumernost), 2 + (1 * soumernost)] - 48;
                    counterNabidekJedenKrok++;
                }
                if (castMapy[2 + (1 * soumernost), 2 - (1 * soumernost)] >= '1' && castMapy[2 + (1 * soumernost), 2 - (1 * soumernost)] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2 - (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 + (1 * soumernost), 2 - (1 * soumernost)] - 48;
                    counterNabidekJedenKrok++;
                }
            }
            if ((Zlodeje.X == 3 && Zlodeje.Y == 2) || (Zlodeje.X == 4 && Zlodeje.Y == 2) || (Zlodeje.X == 0 && Zlodeje.Y == 2) || (Zlodeje.X == 1 && Zlodeje.Y == 2)) //Zlodej je na 3,2 nebo 4,2 a 0,2 a 1,2
            {
                if (Zlodeje.X == 0 || Zlodeje.X == 1)
                {
                    soumernost = -1;
                    for (int y = 0; y < 5; y++)
                    {
                        if (castMapy[4, y] >= '1' && castMapy[4, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                }

                if (Zlodeje.X == 3 || Zlodeje.X == 4)
                {
                    for (int y = 0; y < 5; y++)
                    {
                        if (castMapy[0, y] >= '1' && castMapy[0, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }
                }

                if (castMapy[2 + (1 * soumernost), 1] >= '1' && castMapy[2 + (1 * soumernost), 1] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 + (1 * soumernost), 1] - 48;
                    counterNabidekJedenKrok++;
                }
                if (castMapy[2 + (1 * soumernost), 2] >= '1' && castMapy[2 + (1 * soumernost), 2] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 + (1 * soumernost), 2] - 48;
                    counterNabidekJedenKrok++;
                }
                if (castMapy[2 + (1 * soumernost), 3] >= '1' && castMapy[2 + (1 * soumernost), 3] <= '9')
                {
                    SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2 + (1 * soumernost);
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                    SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2 + (1 * soumernost), 3] - 48;
                    counterNabidekJedenKrok++;
                }
            }

            if ((Zlodeje.X == 3 && Zlodeje.Y == 2) || (Zlodeje.X == 1 && Zlodeje.Y == 4)) // Zlodej se nachazi na pozici 3,0 nebo 1,4
            {
                if (Zlodeje.X == 3)
                {
                    if (castMapy[1, 3] >= '1' && castMapy[1, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 3] >= '1' && castMapy[2, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[3, 3] >= '1' && castMapy[3, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[1, 2] >= '1' && castMapy[1, 2] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 2] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[0, 3] >= '1' && castMapy[0, 3] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, 3] - 48;
                        counterNabidekDvaKroky++;
                    }
                    if (castMapy[0, 2] >= '1' && castMapy[0, 2] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, 2] - 48;
                        counterNabidekDvaKroky++;
                    }
                    for (int x = 0; x < 4; x++)
                    {
                        if (castMapy[x, 4] >= '1' && castMapy[x, 4] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 4] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }

                }

                if (Zlodeje.X == 1)
                {
                    if (castMapy[3, 1] >= '1' && castMapy[3, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 1] >= '1' && castMapy[2, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[3, 2] >= '1' && castMapy[3, 2] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 2] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[1, 1] >= '1' && castMapy[1, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[4, 2] >= '1' && castMapy[4, 2] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, 2] - 48;
                        counterNabidekDvaKroky++;
                    }
                    if (castMapy[4, 1] >= '1' && castMapy[4, 1] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, 1] - 48;
                        counterNabidekDvaKroky++;
                    }
                    for (int x = 1; x < 5; x++)
                    {
                        if (castMapy[x, 0] >= '1' && castMapy[x, 0] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = x;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[x, 0] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }

                }
            }

            if ((Zlodeje.X == 4 && Zlodeje.Y == 1) || (Zlodeje.X == 0 && Zlodeje.Y == 3)) // Zlodej se nachazi na pizici 4,1 nebo 0,3
            {
                if (Zlodeje.X == 4)
                {
                    if (castMapy[1, 3] >= '1' && castMapy[1, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 3] >= '1' && castMapy[2, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[1, 2] >= '1' && castMapy[1, 2] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 2] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[1, 1] >= '1' && castMapy[1, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[1, 1] - 48;
                        counterNabidekJedenKrok++;
                    }

                    if (castMapy[2, 4] >= '1' && castMapy[2, 4] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 2;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[2, 4] - 48;
                        counterNabidekDvaKroky++;
                    }
                    if (castMapy[1, 4] >= '1' && castMapy[1, 4] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 1;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 4;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[1, 4] - 48;
                        counterNabidekDvaKroky++;
                    }
                    for (int y = 1; y < 5; y++)
                    {
                        if (castMapy[0, y] >= '1' && castMapy[0, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 0;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[0, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }

                }

                if (Zlodeje.X == 0)
                {
                    if (castMapy[3, 1] >= '1' && castMapy[3, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 1] >= '1' && castMapy[2, 1] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 1;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[2, 1] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[3, 2] >= '1' && castMapy[3, 2] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 2;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 2] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[3, 3] >= '1' && castMapy[3, 3] <= '9')
                    {
                        SeznamNabidekVnitrni[counterNabidekJedenKrok].X = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].Y = 3;
                        SeznamNabidekVnitrni[counterNabidekDvaKroky].hodnota = castMapy[3, 3] - 48;
                        counterNabidekJedenKrok++;
                    }
                    if (castMapy[2, 0] >= '1' && castMapy[2, 0] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 2;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[2, 0] - 48;
                        counterNabidekDvaKroky++;
                    }
                    if (castMapy[3, 0] >= '1' && castMapy[3, 0] <= '9')
                    {
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 3;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = 0;
                        SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[3, 0] - 48;
                        counterNabidekDvaKroky++;
                    }
                    for (int y = 0; y < 4; y++)
                    {
                        if (castMapy[4, y] >= '1' && castMapy[0, y] <= '9')
                        {
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].X = 4;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].Y = y;
                            SeznamNabidekVnejsi[counterNabidekDvaKroky].hodnota = castMapy[4, y] - 48;
                            counterNabidekDvaKroky++;
                        }
                    }

                }
            }
            if (counterNabidekDvaKroky == 0 && counterNabidekJedenKrok == 0) //nic jsem nenasel
            {
                NabidkaStres.X = 2;
                NabidkaStres.Y = 2;
                return NabidkaStres;
            }
            if ((counterNabidekDvaKroky == 0 && counterNabidekJedenKrok > 0) || (counterNabidekDvaKroky > 0 && counterNabidekJedenKrok > 0)) //nasel jsem bud jenom ve vnitrnim nebo v obojim, tak beru z vnitrniho a prioritizuju nejlevnejsi
            {
                int min = SeznamNabidekVnitrni[0].hodnota;
                int index = 0;
                if (counterNabidekJedenKrok == 1)
                {
                    index = 0;
                }
                else
                {
                    for (int i = 0; i < counterNabidekJedenKrok; i++)
                    {
                        if (min > SeznamNabidekVnitrni[i].hodnota)
                        {
                            index = i;
                            min = SeznamNabidekVnitrni[i].hodnota;
                        }
                    }
                }
                NabidkaStres.X = SeznamNabidekVnitrni[index].X;
                NabidkaStres.Y = SeznamNabidekVnitrni[index].Y;
                return NabidkaStres;
            }
            if (counterNabidekDvaKroky > 0 && counterNabidekJedenKrok == 0) //nasel jsem ve vnejsim, hledam nejlevnejsi a vracim souradnice
            {
                int min = SeznamNabidekVnejsi[0].hodnota;
                int index = 0;
                if (counterNabidekDvaKroky == 1)
                {
                    index = 0;
                }
                else
                {
                    for (int i = 0; i < counterNabidekDvaKroky; i++)
                    {
                        if (min > SeznamNabidekVnejsi[i].hodnota)
                        {
                            index = i;
                            min = SeznamNabidekVnejsi[i].hodnota;
                        }
                    }
                }
                NabidkaStres.X = SeznamNabidekVnejsi[index].X;
                NabidkaStres.Y = SeznamNabidekVnejsi[index].Y;
                return NabidkaStres;
            }

            return NabidkaStres;
        }
        private Smer VyhodnotCestuSeZdmi(char[,] castMapy, Souradnice zlodej, Souradnice nabidka)
        {
            //vyhodnoti nejlepsi cestu abychom se zaroven dostali k nabidce a zaroven stali nebyli na dosah zlodeje
            if ((nabidka.X == 3 && nabidka.Y == 0) || (nabidka.X == 4 && nabidka.Y == 0) || (nabidka.X == 4 && nabidka.Y == 1))
            {
                if (castMapy[3, 1] == ' ')
                {
                    return new Smer(1, -1);
                }
            }
            if ((nabidka.X == 4 && nabidka.Y == 3) || (nabidka.X == 4 && nabidka.Y == 4) || (nabidka.X == 3 && nabidka.Y == 4))
            {
                if (castMapy[3, 3] == ' ')
                {
                    return new Smer(1, 1);
                }
            }
            if ((nabidka.X == 1 && nabidka.Y == 4) || (nabidka.X == 0 && nabidka.Y == 4) || (nabidka.X == 0 && nabidka.Y == 3))
            {
                if (castMapy[1, 3] == ' ')
                {
                    return new Smer(-1, 1);
                }
            }
            if ((nabidka.X == 0 && nabidka.Y == 1) || (nabidka.X == 0 && nabidka.Y == 0) || (nabidka.X == 1 && nabidka.Y == 0))
            {
                if (castMapy[1, 1] == ' ')
                {
                    return new Smer(-1, -1);
                }
            }
            if (nabidka.X == 2 && nabidka.Y == 0 && castMapy[2, 1] == ' ')
            {
                return new Smer(0, -1);
            }
            if (nabidka.X == 4 && nabidka.Y == 2 && castMapy[3, 2] == ' ')
            {
                return new Smer(1, 0);
            }
            if (nabidka.X == 2 && nabidka.Y == 4 && castMapy[2, 3] == ' ')
            {
                return new Smer(0, 1);
            }
            if (nabidka.X == 0 && nabidka.Y == 2 && castMapy[1, 2] == ' ')
            {
                return new Smer(-1, 0);
            }
            return new Smer(0, 0);
        }
    }
}
