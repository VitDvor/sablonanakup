﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{
    class Nakupujici2 : Nakupujici
    {
        private int pocet_kol;
        private char[,] mapa = new char[80, 20];
        private int zaplneniZ;
        private int zaplneniN;
        private bool check;
        private bool poprve = true;
        Smer pomocna = new Smer { X = 0, Y = 0 };
        private Smer[] PredchoziTahy = new Smer[8];
        public Nakupujici2()
        {
            pocet_kol = 1;
            Nabidky[] nabidky = new Nabidky[25];

        }
        //  NovaNabidka ziska jakou hodnotu ma  nabidka
        // a na jakych je souradnicich i, j
        // vytvori novou nabidku a vrati ji
        Nabidky NovaNabidka(int nabidka, int i, int j)
        {
            Nabidky nova;
            nova.kolo = pocet_kol;
            nova.cena = nabidka;
            nova.x = i;
            nova.y = j;
            return nova;
        }
        //Nabidka dostane cast mapy, pole na pripadne nabidky a kolik policek je zaplnenych (0)
        //zjisti zda mapa obsahuje nabidky a pripadne je zapise do pole nabidek
        // vraci true pokud nasla nabidku
        public Nabidky[] NajdiN(char[,] castMapy)
        {
            Nabidky[] nabidky = new Nabidky[25];
            int zaplneni = 0;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (castMapy[i, j] > 0 && castMapy[i, j] < 10)
                    {
                        nabidky[zaplneni].x = i;
                        nabidky[zaplneni].y = j;
                        nabidky[zaplneni].cena = castMapy[i, j];
                        zaplneni++;
                    }
                }
            }
            zaplneniN = zaplneni;
            return nabidky;
        }
        public Souradnice VyberCil(Nabidky[] seznam)
        {
            Souradnice cil = new Souradnice();
            int cena = 10;
            foreach (Nabidky nabidky in seznam)
            {
                if (nabidky.cena < cena)
                {
                    cena = nabidky.cena;
                    cil.X = nabidky.x;
                    cil.Y = nabidky.y;
                }
            }
            return cil;
        }
        public Zlodejik[] NajdiZ(char[,] castMapy)
        {
            Zlodejik[] zlodeji = new Zlodejik[25];
            int zaplneni = 0;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (castMapy[i, j] >= 'A' && castMapy[i, j] < 'X')
                    {
                        zlodeji[zaplneni].x = i;
                        zlodeji[zaplneni].y = j;
                        zaplneni++;
                    }
                }
            }
            zaplneniZ = zaplneni;
            return zlodeji;
        }
        public Smer Oprava(Smer odpoved, char[,] castMapy, Souradnice souradnice)
        {
            int pokus = 1;
            while (check != false)
            {
                if (odpoved.X == 0 && pokus == 1) { odpoved.X = 1; pokus++; }
                else if (odpoved.X == 0) { odpoved.X = -1; }
                else if (odpoved.Y == 0 && pokus == 1) { odpoved.Y = 1; pokus++; }
                else if (odpoved.Y == 0) { odpoved.Y = -1; }
                else if (((odpoved.X == odpoved.Y) || (odpoved.X == -1 * odpoved.Y)) && pokus == 1) { odpoved.X = 0; pokus++; }
                else if ((odpoved.X == odpoved.Y) || (odpoved.X == -1 * odpoved.Y)) { odpoved.Y = 0; }
                check = Kontrola(castMapy, odpoved, souradnice);
            }
            return odpoved;
        }
        Smer krokOdZlodeje(char[,] castMapy, Zlodejik[] seznamZlodeju, int pocetZ, Souradnice souradnice)
        {
            Smer odpoved = new Smer();
            if (pocetZ == 1)
            {
                if (seznamZlodeju[0].x > 2) { odpoved.X = -1; }
                else if (seznamZlodeju[0].x < 2) { odpoved.X = 1; }
                else { odpoved.X = 0; }
                if (seznamZlodeju[0].y > 2) { odpoved.Y = -1; }
                else if (seznamZlodeju[0].y < 2) { odpoved.Y = 1; }
                else { odpoved.Y = 0; }
            }
            else
            {
                JdemNekam(castMapy, seznamZlodeju, 0, souradnice);
            }
            check = Kontrola(castMapy, odpoved, souradnice);
            if (check == true) { odpoved = Oprava(odpoved, castMapy, souradnice); }
            PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            return odpoved;
        }
        public Smer JdemPoNabidce(char[,] castMapy, Nabidky[] seznamNabidek, int pocetN, Zlodejik[] seznamZlodeju, int pocetZ, int penize, Souradnice souradnice)
        {
            Smer odpoved = new Smer();
            if (pocetZ != 0)
            {
                krokOdZlodeje(castMapy, seznamZlodeju, pocetZ, souradnice);
            }
            else
            {
                Souradnice cil = VyberCil(seznamNabidek);
                if (cil.X > 2) { odpoved.X = 1; }
                else if (cil.X < 2) { odpoved.X = -1; }
                else { odpoved.X = 0; }
                if (cil.Y > 2) { odpoved.Y = 1; }
                else if (cil.Y < 2) { odpoved.Y = -1; }
                else { odpoved.Y = 0; }
            }
            check = Kontrola(castMapy, odpoved, souradnice);
            if (check == true) { odpoved = Oprava(odpoved, castMapy, souradnice); }
            PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            return odpoved;
        }
        public Smer JdemNekam(char[,] castMapy, Zlodejik[] seznamZlodeju, int pocetZ, Souradnice souradnice)
        {
            Smer odpoved = new Smer();
            if (pocetZ != 0)
            {
                krokOdZlodeje(castMapy, seznamZlodeju, pocetZ, souradnice);
            }
            else
            {
                if (poprve == true) { odpoved.X = 1; odpoved.Y = 1; poprve = false; }
                else { odpoved = PredchoziTahy[7]; }
            }
            check = Kontrola(castMapy, odpoved, souradnice);
            if (check == true) { odpoved = Oprava(odpoved, castMapy, souradnice); }
            PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            return odpoved;
        }
        bool Kontrola(char[,] castMapy, Smer odpoved, Souradnice souradnice)
        {
            bool kontrolovani = false;
            int x = (sbyte)(odpoved.X + 3);
            int y = (sbyte)(odpoved.Y + 3);
            if (castMapy[x, y] >= 'A' && castMapy[x, y] >= 'z') { kontrolovani = true; }
            return kontrolovani;
        }

        //dela krok nakupujiciho
        //deli se podle toho zda jsou nebo nejsou nabidky
        //na konci pricte pocet kol
        public override Smer Krok(char[,] castMapy, int penize, Souradnice souradnice)
        {
            Smer odpoved;
            Zlodejik[] seznamZlodeju = new Zlodejik[25];
            Nabidky[] seznamNabidek = new Nabidky[25];
            zaplneniZ = 0;
            zaplneniN = 0;
            seznamZlodeju = NajdiZ(castMapy);
            seznamNabidek = NajdiN(castMapy);
            if (zaplneniN > 0)
            {
                odpoved = JdemPoNabidce(castMapy, seznamNabidek, zaplneniN, seznamZlodeju, zaplneniZ, penize, souradnice);
            }
            else
            {
                odpoved = JdemNekam(castMapy, seznamZlodeju, zaplneniZ, souradnice);
            }
            pocet_kol++;
            PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            return odpoved;
        }
        public Smer[] ZapisTah(Smer[] PT, Smer odpoved)
        {
            for (int i = 0; i < 7; i++)
            {
                PT[i] = PT[i + 1];
            }
            PT[7] = odpoved;
            return PT;
        }
    }
    struct Zlodeji
    {
        public Zlodejik[] zlodeji;
        public int zaplneni;
        public Zlodeji(Zlodejik[] zlodeji, int zaplneni)
        {
            this.zlodeji = zlodeji;
            this.zaplneni = zaplneni;
        }

    }
    struct Zlodejik
    {
        public int x, y;
        public Zlodejik(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
    struct Nabidky
    {
        public int kolo, cena, x, y;

        public Nabidky(int kolo, int cena, int x, int y)
        {
            this.kolo = kolo;
            this.cena = cena;
            this.x = x;
            this.y = y;

        }
    }

}
