﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{
    class Shopper : Nakupujici
    {
        public Shopper()
        {
        }

        //trash holdy pohybu
        private readonly int u_trash = 77;
        private readonly int d_trash = 2;
        private readonly int r_tash = 17;
        private readonly int l_trash = 2;

        //kontrola zda Nakupujici dorazil do předem vybraného cíle
        private int cil = 0;
        private Souradnice cilova = new Souradnice(0, 0);

        /// <summary>
        /// nastaví směr pohybu bez ohledu na okolí
        /// </summary>
        /// <returns></returns>
        private Smer ZakladniSmer()
        {
            return (new Smer((sbyte)(nahoda.Next(3) - 1), (sbyte)(nahoda.Next(3) - 1)));
        }

        private Smer NajdiZlodeje(char[,] mapka)
        {
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    if ((int)mapka[x, y] > 64 && (int)mapka[x, y] < 91)
                        return new Smer((sbyte)(2 - x), (sbyte)(2 - y));
                }
            }

            return new Smer(0, 0);
        }

        private int MaxCena(int penize)
        {
            if (penize > 0 && (penize / 5) == 0)
                return 1;
            else
                return (penize / 5);
        }

        /// <summary>
        /// Ve viditelnem okruhu najde vhodnou nabídku.
        /// </summary>
        /// <param name="mapka">Kousek videne mapy</param>
        /// <param name="penize">Prostredky pro nakup</param>
        /// <returns></returns>
        private Smer NajdiNabidku(char[,] mapka, int penize)
        {
            Smer koupit;
            int poptavka = MaxCena(penize);

            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    if ((int)mapka[x, y] <= poptavka)
                    {
                        koupit = new Smer((sbyte)x, (sbyte)y);
                        return koupit;
                    }
                    else if (x == 3 && y == 3)
                    {
                        koupit = new Smer(0, 0);
                        return koupit;
                    }
                }
            }

            koupit = new Smer(0, 0);
            return koupit;
        }

        public override Smer Krok(char[,] castMapy, int penize, Souradnice mojeSouradnice)
        {
            Smer step = ZakladniSmer();
            Smer utek = NajdiZlodeje(castMapy);
            Smer nakup = NajdiNabidku(castMapy, penize);

            if (utek.X == 0 && utek.Y == 0)
                ;
            else
            {
                return utek;
            }

            if (nakup.X == 0 && nakup.Y == 0)
                ;
            else
            {
                return nakup;
            }

            return step;
        }
    }
}
