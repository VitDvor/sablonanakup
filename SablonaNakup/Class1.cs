﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{

    public struct Sleva
    {
        public int x, y;
        public int hodnota;
        public bool vnitrni_kruh;   //false = vnejsi
        public bool je_zlodej;
    }

    public struct SmerKroku
    {
        public int x, y;
    }




    class SaleHunter24_7 : Nakupujici
    {


        static int range_x = 80, range_y = 20;
        char[,] mojemapa = new char[range_x + 4, range_y + 4];     //rozmery mapy
        bool inicializace = false;
        int pocet_kol = 0;
        SmerKroku PosledniKrok = new SmerKroku();   // SMER!!!!
        static SmerKroku PrepisPosledniKrok(Sleva starejKrok)
        {
            SmerKroku cil;
            cil.x = starejKrok.x;
            cil.y = starejKrok.y;
            return cil;
        }
        private bool jaky_kruh(Sleva cil)
        {
            if (cil.x == 2 || cil.x == -2 || cil.y == 2 || cil.y == -2)
            {
                return false;
            }
            return true;
        }
        public override Smer Krok(char[,] castMapy, int penize, Souradnice mojeSouradnice)
        {
            //------------------ inicializace ------------------------------------


            Sleva[] slevy = new Sleva[40];
            Sleva[] vymaz = new Sleva[25];
            int pocet_vymaz = 0;
            int pocet_slev = 0;
            bool prelozeno = false;
            if (!inicializace)
            {
                PosledniKrok.x = 1;
                PosledniKrok.y = 1;
                //Ukladame si posledni krok, nejprve to bude 1,1 
                for (int i = 0; i < mojemapa.GetLength(1); i++)
                {
                    for (int ii = 0; ii < mojemapa.GetLength(0); ii++)
                    {
                        mojemapa[ii, i] = '?';
                    }

                }
                inicializace = true;
            }
            // ---------------------------------- rozebraní mapy --------------------------
            // ii X souradnice ;i Y souradnice; castMapy[ii,i] 
            for (int i = 0; i < 5; i++)
            {
                for (int ii = 0; ii < 5; ii++)
                {
                    if (castMapy[ii, i] != '?')
                    {
                        if ((int)castMapy[ii, i] < 58 && (int)castMapy[ii, i] > 48)
                        {
                            slevy[pocet_slev].x = mojeSouradnice.X + ii - 2;
                            slevy[pocet_slev].y = mojeSouradnice.Y + i - 2;
                            slevy[pocet_slev].hodnota = (int)castMapy[ii, i] - 48;
                            slevy[pocet_slev].vnitrni_kruh = jaky_kruh(slevy[pocet_slev]);
                            pocet_slev++;

                        }
                        else if ((int)castMapy[ii, i] >= 65 && (int)castMapy[ii, i] <= 90 && castMapy[ii, i] != 'X' && ii != 2 && i != 2)
                        {

                            // ZLODEJ
                            castMapy[ii, i] = 'Z';
                            vymaz[pocet_vymaz].x = mojeSouradnice.X + ii - 2;
                            vymaz[pocet_vymaz].y = mojeSouradnice.Y + i - 2;
                            vymaz[pocet_vymaz].je_zlodej = true;
                            pocet_vymaz++;


                        }
                        else if ((int)castMapy[ii, i] >= 97 && (int)castMapy[ii, i] <= 122)
                        {
                            // NAKPUJICI
                            castMapy[ii, i] = 'N';
                            vymaz[pocet_vymaz].x = mojeSouradnice.X + ii - 2;
                            vymaz[pocet_vymaz].y = mojeSouradnice.Y + i - 2;
                            pocet_vymaz++;



                        }
                        castMapy[2, 2] = 'V'; // já      Véčko!!
                        mojemapa[mojeSouradnice.X + ii, mojeSouradnice.Y + i] = castMapy[ii, i];   // -2 + 2 v souradnicich jelikoz -2 vyrez a +2 okraje mapy
                    }
                }
            }
            //---------------- nevim-----------------------------------
            /*
             * Pokud bude okolo slevy v rangy bliz nez V Nakupujici -> kaslem na to
             * Pokud bude okolo slevy v rangy bliz nez V Zlodej - > přičtem 5 ke slevě
             *To se uplne nepovedlo mno
             * 
             * 
             * 
             * 
             * 
             */
            // ---- strategie------
            int maximalni_sleva = 5 + (pocet_kol / 25);


            // --------Vyberem slevu nebo krok-----
            Sleva cilova_sleva = new Sleva();
            cilova_sleva.hodnota = 10; //10 nejde -> tzn sleva neni
                                       // Kuba kouzlil
            if (mojemapa[mojeSouradnice.X + PosledniKrok.x + 2, mojeSouradnice.Y + PosledniKrok.y + 2] == 'X' || mojemapa[mojeSouradnice.X + PosledniKrok.x + 2, mojeSouradnice.Y + PosledniKrok.y + 2] == 'N')
            {
                if (mojeSouradnice.X > 40)
                {
                    cilova_sleva.x = mojeSouradnice.X - 20;
                }
                else
                {
                    cilova_sleva.x = mojeSouradnice.X + 20;
                }
                if (mojeSouradnice.Y > 10)
                {
                    cilova_sleva.y = mojeSouradnice.Y - 5;
                }
                else
                {
                    cilova_sleva.y = mojeSouradnice.Y + 5;
                }
                Sleva lokalni;
                if (cilova_sleva.x - mojeSouradnice.X > 0)
                {
                    lokalni.x = 1;
                }
                else if (cilova_sleva.x == mojeSouradnice.X)
                {
                    lokalni.x = 0;
                }
                else
                {
                    lokalni.x = -1;
                }
                if (cilova_sleva.y - mojeSouradnice.Y > 0)
                {
                    lokalni.y = 1;
                }
                else if (cilova_sleva.y == mojeSouradnice.Y)
                {
                    lokalni.y = 0;
                }
                else
                {
                    lokalni.y = -1;
                }
                if (lokalni.x == -PosledniKrok.x && lokalni.y == -PosledniKrok.y)
                {
                    cilova_sleva.x = nahoda.Next(3) - 1; // kdyby se nic neprotlo
                    cilova_sleva.y = nahoda.Next(3) - 1; // same
                    if (cilova_sleva.x == -PosledniKrok.x || cilova_sleva.y == -PosledniKrok.y)
                    {
                        cilova_sleva.x = nahoda.Next(3) - 1; // kdyby se nic neprotlo
                        cilova_sleva.y = nahoda.Next(3) - 1; // same
                    }
                    prelozeno = true;
                }

            }
            else
            {
                cilova_sleva.x = PosledniKrok.x;
                cilova_sleva.y = PosledniKrok.y;
                prelozeno = true;

            }
            if (prelozeno)
            {
                while ((cilova_sleva.x == 0 && cilova_sleva.y == 0) || (mojemapa[mojeSouradnice.X + cilova_sleva.x + 2, mojeSouradnice.Y + cilova_sleva.y + 2] == 'X' || mojemapa[mojeSouradnice.X + cilova_sleva.x + 2, mojeSouradnice.Y + cilova_sleva.y + 2] == 'N'))
                {
                    cilova_sleva.x = nahoda.Next(3) - 1; // kdyby se nic neprotlo
                    cilova_sleva.y = nahoda.Next(3) - 1; // same

                }
            }
            else
            {
                Sleva lokalni;
                if (cilova_sleva.x - mojeSouradnice.X > 0)
                {
                    lokalni.x = 1;
                }
                else if (cilova_sleva.x == mojeSouradnice.X)
                {
                    lokalni.x = 0;
                }
                else
                {
                    lokalni.x = -1;
                }
                if (cilova_sleva.y - mojeSouradnice.Y > 0)
                {
                    lokalni.y = 1;
                }
                else if (cilova_sleva.y == mojeSouradnice.Y)
                {
                    lokalni.y = 0;
                }
                else
                {
                    lokalni.y = -1;
                }
                while ((cilova_sleva.x == 0 && cilova_sleva.y == 0) || (mojemapa[mojeSouradnice.X + lokalni.x + 2, mojeSouradnice.Y + lokalni.y + 2] == 'X' || mojemapa[mojeSouradnice.X + lokalni.x + 2, mojeSouradnice.Y + lokalni.y + 2] == 'N'))
                {
                    lokalni.x = nahoda.Next(3) - 1; // kdyby se nic neprotlo
                    lokalni.y = nahoda.Next(3) - 1; // same

                }
                cilova_sleva.x = lokalni.x;
                cilova_sleva.y = lokalni.y;
                prelozeno = true;
            }


            //Véčko se bude snažit držet svůj směr, dokud to pude
            //Když narazí na zeď nebo nakupujícího, tak vyrazí ke středu
            //Když i tam bude zeď, tak pude na fandu random


            //cilova_sleva.x = nahoda.Next(mojemapa.GetLength(0)-4) + 2; // kdyby se nic neprotlo
            //cilova_sleva.y = nahoda.Next(mojemapa.GetLength(1)-4) + 2; // same
            // az sem
            for (int i = 0; i < pocet_slev; i++)
            {
                if (slevy[i].hodnota <= maximalni_sleva && penize >= slevy[i].hodnota)
                {
                    if (slevy[i].hodnota < cilova_sleva.hodnota)
                    {
                        cilova_sleva = slevy[i];
                    }
                    else if (slevy[i].hodnota == cilova_sleva.hodnota && slevy[i].vnitrni_kruh)
                    {
                        cilova_sleva = slevy[i];
                    }
                    prelozeno = false;
                }
            }
            if (cilova_sleva.hodnota == 10)
            {
                for (int i = 0; i < pocet_vymaz; i++)
                {
                    if (vymaz[i].je_zlodej)
                    {
                        if (vymaz[i].x < 0)
                        {
                            cilova_sleva.x = 1;
                        }
                        else
                        {
                            cilova_sleva.x = -1;
                        }

                        if (vymaz[i].y < 0)
                        {
                            cilova_sleva.y = 1;
                        }
                        else
                        {
                            cilova_sleva.y = -1;
                        }
                        cilova_sleva.hodnota = 1;
                        prelozeno = true;

                    }
                }
            }
            //---- prevedu cilovou slevu na pohyb
            if (!prelozeno)
            {
                if (cilova_sleva.x < mojeSouradnice.X)
                {
                    cilova_sleva.x = -1;
                }
                else if (cilova_sleva.x == mojeSouradnice.X)
                {
                    cilova_sleva.x = 0;
                }
                else
                {
                    cilova_sleva.x = 1;
                }

                if (cilova_sleva.y < mojeSouradnice.Y)
                {
                    cilova_sleva.y = -1;
                }
                else if (cilova_sleva.y == mojeSouradnice.Y)
                {
                    cilova_sleva.y = 0;
                }
                else
                {
                    cilova_sleva.y = 1;
                }
                PosledniKrok = PrepisPosledniKrok(cilova_sleva);
            }
            else
            {
                PosledniKrok = PrepisPosledniKrok(cilova_sleva);
            }
            // ----- ZED FIX-----
            while (castMapy[cilova_sleva.x + 2, cilova_sleva.y + 2] == 'X' || castMapy[cilova_sleva.x + 2, cilova_sleva.y + 2] == 'N')
            {
                cilova_sleva.x = nahoda.Next(3) - 1; // kdyby se nic neprotlo
                cilova_sleva.y = nahoda.Next(3) - 1; // same

            }




            //------- ukonceni Kroku--------
            pocet_kol++;
            for (int i = 0; i < pocet_vymaz; i++)
            {
                mojemapa[vymaz[i].x, vymaz[i].y] = ' ';
                // promazani starych postav
            }

            return new Smer((sbyte)cilova_sleva.x, (sbyte)cilova_sleva.y);
        }
    }
}
